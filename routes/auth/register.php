<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->post('/register/', function () use ($app) {

	if(valid_parameters("first_name,last_name,city,state,country,zip_code,email,password,gender,birth_date,confirm_password,confirm_email","POST"))
	{
		global $db;

		$first_name = $app->request->post('first_name');
		$last_name = $app->request->post('last_name');
		$city = $app->request->post('city');
		$state = $app->request->post('state');
		$country = $app->request->post('country');
		$zip_code = $app->request->post('zip_code');
		$gender = $app->request->post('gender');
		$birth_date = $app->request->post('birth_date');
		$email = $app->request->post('email');
		$password = $app->request->post('password');
		$confirm_password = $app->request->post('confirm_password');
		$confirm_email = $app->request->post('confirm_email');
		$hashed = secure_password($password);

		if(($confirm_password!=$password) || ($confirm_email!=$email))
		{
			write("Please check email or password",true,200);
		}



		$id = $db->insert("users", [
	"first_name" => $first_name,
	"last_name" => $last_name,
	"city" => $city,
	"state" => $state,
	"country" => $country,
	"zip_code" => $zip_code,
	"email" => $email,
	"salt" => $hashed[1],
	"password"=> $hashed[0],
	"gender" => $gender,
	"birth_date" => $birth_date,
	"#created_on"=> "NOW()",
	"#updated_on" => "NOW()"]);
		if($id == 0)
		{

			write($db->error()[2],true,200);
			return;
		}

		

	$output['user'] = array(
		"id" => $id,
		"first_name" => $first_name,
	"last_name" => $last_name,
		"email" => $email,
		"links" => getLinks($id,"user")
		);

	$hive_id = $db->insert("hives",["owner_id" => $id,
		"#created_on" => "NOW()"]);

	$db->insert("hive_members",["user_id" => $id,
		"hive_id" => $hive_id,
		"status" => 0,
		"#created_date" => "NOW()"]);
	
	write($output,false,201);

	}

});