<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//Login END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/login/
Allowed : POST
Parameters Requried: email, password

Returned Output:

{
    "user": {
        "id": "36",
        "email": "enarsee@gmail.com",
        "api_key": "ddbbf2f1d6c1ecc",
        "expiry_time": "2014-12-04 04:53:30",
        "links": {
            "self": "/user/36",
            "brands": "/user/36/brands",
            "favourites": "/user/36/favourites"
        }
    },
    "error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->post('/login/', function() use($app)
{
	if(valid_parameters("email,password","POST"))
	{
		global $db;
		$email = $app->request->post('email');
		$password = $app->request->post('password');
		$login = check_login($email,$password);

		if(is_array($login))
		{
			$output['user'] = array(
				"id" => $login[0],
				"email" => $email,
				"api_key" => $login[1],
				"expiry_time" => $login[2],
				"links" =>getLinks($login[0],"user")
		);

			


			write($output,false,200);
			return;
		}
		else
		{
			write("Wrong Username/Password",true,200);
		}
	}
});