<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING
$app->get("/brands/","authenticate",function() use($app){
global $db;


$brands = $db->select("brands",[
	"id", "name", "description", "photo", "created_on"]);


$output = array();
$output['brands'] = array();

foreach ($brands as $brand)
{
	

	$brand['links'] = getLinks($brand['id'],"brand");
	array_push($output['brands'],$brand);
}

$output['meta']["total"] = count($output['brands']);

write($output,false,200);
});


$app->post("/brands/", function(){
	write("Not Allowed",true,405);
});

$app->get("/brand/:id", "authenticate",function($id){

	global $db;
	$brands = $db->select("brands",[
	"id", "name", "description", "photo", "created_on"],["id" => $id]);


	$output['brand'] = array();


if(count($brands)>0)
{
	$brand = $brands[0];
	$brand["level_info"] = getBrandLevel($brand['id']);
	$brand["links"] = getLinks($brand['id'],"brand");
	
	$output['brand'] = $brand;
	write($output,false,200);
}
else
{
	write("Brand Not Found",true,200);
}

});

$app->put("/brand/:id", function($id){
	write("Not Implemented",true,302);
});

$app->post("/brand/:id", function($id){
	write("Not Implemented",true,302);
});

$app->patch("/brand/:id", function($id){
	write("Not Implemented",true,302);
});

