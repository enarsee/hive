<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->put("/plugin", "plugin_authenticate",function() use($app){

	global $db;
	global $plugin_id;

	$db->update("plugins",["is_active" => 1],["id"=>$plugin_id]);

	if(!is_db_error())
		write("Plugin Activated Successfully",false,200);

});

$app->delete("/plugin", "plugin_authenticate",function() use($app){

	global $db;
	global $plugin_id;

	$db->update("plugins",["is_active" => 0],["id"=>$plugin_id]);

	if(!is_db_error())
		write("Plugin De-activated Successfully",false,200);

});

$app->post("/plugin/purchases","plugin_authenticate",function() use($app){

	global $db;
	global $plugin_id;

	//Get brand
	$plugin = $db->get("plugins",["brand_id"],["id" => $plugin_id]);
	$brand_id = $plugin["brand_id"];

	if(valid_parameters("user_id,item_id,item_amount,honey_applied","POST"))
	{
		$user_id = $app->request->post("user_id");
		$item_id = $app->request->post("item_id");
		$item_amount = $app->request->post("item_amount");
		$honey_applied = (float)($app->request->post("honey_applied"));

		if(($honey_applied == 0) || has_honey($user_id,$brand_id,$honey_applied))
		{
		//Subtract Honey Applied
		if($honey_applied != 0)
			subtract_honey($user_id,$brand_id,$honey_applied);

		$honey = $db->get("honey",["honey"],["AND" => ["user_id" => $user_id,
			"brand_id" => $brand_id]]);

		//Get current level
		$level_info = getLevel($honey["honey"],$brand_id);
		$earning_percentage = (float)$level_info["earning_percentage"];

		//Calculate Honey Gained
		$honey_eligible = (float)$item_amount - (float)$honey_applied;

		$honey_gained = $honey_eligible*($earning_percentage/100);

		//Add Honey Gained
		add_honey($user_id,$brand_id,$honey_gained);

		//Add to Purchase
		$purchase_id = $db->insert("purchases",
			["user_id" => $user_id,
			"brand_id" => $brand_id,
			"plugin_id" => $plugin_id,
			"item_id" => $item_id,
			"item_amount" => $item_amount,
			"honey_applied" => $honey_applied,
			"honey_gained" => $honey_gained,
			"#purchased_on" => "NOW()"]);

		if(!is_db_error())
		{
			$purchase_data = $db->get("purchases",["user_id",
				"brand_id",
				"plugin_id",
				"item_id",
				"item_amount",
				"honey_applied",
				"honey_gained",
				"purchased_on"],[
				"id" => $purchase_id]);

			$output["purchase"] = $purchase_data;

			write($output,false,201);
		}
	}
	else
	{
		write("Invalid Request",true,200);
	}
	}

	//Get User ID from plugin

	//Get details and input them


});

