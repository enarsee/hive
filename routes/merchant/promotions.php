<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->post("/promotions", "plugin_authenticate",function() use($app){

	global $db;
	global $plugin_id;

	//Get brand
	$plugin = $db->get("plugins",["brand_id"],["id" => $plugin_id]);
	$brand_id = $plugin["brand_id"];

	//POST Promotions
	$parameters = $app->request->post();
	$promotion = $app->request->post("promotion");
	unset($parameters["promotion"]);

	$query_array= array();
	if(count($parameters) > 1)
		{
			 	$query_array["AND"] = array();
			 	$query_array["AND"] = $parameters;
		}
		else
			$query_array = $parameters;
	


	
	$users = $db->select("users",["id"],$query_array);
	


	foreach($users as $user)
	{
		$db->insert("promotions",[
			"brand_id" => $brand_id,
			"user_id" => $user["id"],
			"promotion" => $promotion,
			"#added_on" => "NOW()"]);

	}

	if(!is_db_error())
	{
		write("Added Successfully",false,200);
	}
	

});


