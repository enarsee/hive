<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->post("/ads", "plugin_authenticate",function() use($app){

	global $db;
	global $plugin_id;

	//Get brand
	$plugin = $db->get("plugins",["brand_id"],["id" => $plugin_id]);
	$brand_id = $plugin["brand_id"];

	//POST Ads
	if(valid_parameters("content,link,image","POST"))
	{
	$content = $app->request->post("content");
	$link = $app->request->post("link");
	$image = $app->request->post("image");

	$db->insert("ads",
		["brand_id"=>$brand_id,
		"content"=>$content,
		"link"=>$link,
		"image"=>$image,
		"#date_added"=>"NOW()"]);

	if(!is_db_error())
	{
		write("Added successfully",false,200);
	}
}

	

});


