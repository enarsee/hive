<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING



$app->get("/ads", "authenticate",function() use($app){

	write(get_ads(),false,200);
});



$app->get("/ad/:aid", "authenticate",function($aid) use($app){
	$output = get_ad($aid);
	

	write($output,false,200);
});

$app->put("/ad/:aid", "authenticate",function($id,$brand) use($app){
	write("Not implemented",true,501);
});

$app->delete("/ad/:aid", "authenticate",function($id,$brand) use($app){
	write("Not implemented",true,501);
});