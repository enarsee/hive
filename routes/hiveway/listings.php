<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/listings", "authenticate",function() use($app){

	global $db;

	$listings = $db->select("listings",
		["id","user_id","offered_brand","offered_honey","requested_brand","requested_honey","listed_on"],["is_hidden" => 0]);


	foreach($listings as $key=>$listing)
	{
		$listings[$key]["lister_info"] = get_user_data($listing["user_id"]);


		$listings[$key]["offered_brand"] = get_brand_info($listing["offered_brand"]);
		$listings[$key]["offered_brand"]["offered_honey"] = $listing["offered_honey"];
		unset($listings[$key]["offered_honey"]);

		$listings[$key]["requested_brand"] = get_brand_info($listing["requested_brand"]);
		$listings[$key]["requested_brand"]["requested_honey"] = $listing["requested_honey"];
		unset($listings[$key]["requested_honey"]);
	}

	$output["listings"]  = $listings;
	$output["meta"]["total"] = count($listings);

	write($output,false,200);

});

$app->get("/listings/fav", "authenticate",function() use($app){

	global $db;
	global $user_id;

	$listings = $db->select("listings",
		["id","user_id","offered_brand","offered_honey","requested_brand","requested_honey","listed_on"],["is_hidden" => 0]);

	//Get Fav
	$fav = $db->select("favourites",["brand_id",
		"user_id"],["user_id"=>$user_id]);


	$fixed_listings = array();

	foreach($listings as $key=>$listing)
	{
		//Filter by fav
		

		if((in_array_multi($fav,"brand_id",(int)$listing["offered_brand"])))
		{
			$listings[$key]["lister_info"] = get_user_data($listing["user_id"]);


			$listings[$key]["offered_brand"] = get_brand_info($listing["offered_brand"]);
			$listings[$key]["offered_brand"]["offered_honey"] = $listing["offered_honey"];
			unset($listings[$key]["offered_honey"]);

			$listings[$key]["requested_brand"] = get_brand_info($listing["requested_brand"]);
			$listings[$key]["requested_brand"]["requested_honey"] = $listing["requested_honey"];
			unset($listings[$key]["requested_honey"]);
			
			array_push($fixed_listings, $listings[$key]);
		}
		

		

		
	}

	$output["listings"]  = $fixed_listings;
	$output["meta"]["total"] = count($fixed_listings);

	write($output,false,200);

});

$app->get("/listings/search", "authenticate",function() use($app){

	global $db;

	$parameters = $app->request->get();
	

	$query_array["AND"]= array();
	$query_array["AND"] = $parameters;
	$query_array["AND"]["is_hidden"] = 0;




	$listings = $db->select("listings",
		["id","user_id","offered_brand","offered_honey","requested_brand","requested_honey","listed_on"],$query_array);



	foreach($listings as $key=>$listing)
	{
		$listings[$key]["lister_info"] = get_user_data($listing["user_id"]);


		$listings[$key]["offered_brand"] = get_brand_info($listing["offered_brand"]);
		$listings[$key]["offered_brand"]["offered_honey"] = $listing["offered_honey"];
		unset($listings[$key]["offered_honey"]);

		$listings[$key]["requested_brand"] = get_brand_info($listing["requested_brand"]);
		$listings[$key]["requested_brand"]["requested_honey"] = $listing["requested_honey"];
		unset($listings[$key]["requested_honey"]);
	}

	$output["listings"]  = $listings;
	$output["meta"]["total"] = count($listings);


	write($output,false,200);

});

$app->post("/listings", "authenticate",function() use($app){

	global $db;
	global $user_id;

	if(valid_parameters("offered_brand,offered_honey,requested_brand,requested_honey","POST"))
	{
		$offered_brand = $app->request->post("offered_brand");
		$offered_honey = $app->request->post("offered_honey");
		$requested_brand = $app->request->post("requested_brand");
		$requested_honey = $app->request->post("requested_honey");


		if(has_honey($user_id,$offered_brand,$offered_honey))
		{

			

			$inserted = $db->insert("listings",
				["user_id" => $user_id,
				"offered_brand" => $offered_brand,
				"offered_honey" => $offered_honey,
				"requested_brand" => $requested_brand,
				"requested_honey" => $requested_honey,
				"#listed_on" => "NOW()"]);


			lock_honey($user_id,$offered_brand,$offered_honey,1,$inserted);

			$inserted_data = $db->get("listings","*",["id" => (int)$inserted]);

			

			$output["listing"] = $inserted_data;

			write($output,false,201);
		}
	}

});

$app->get("/listing/:lid", "authenticate",function($lid) use($app){

	global $db;

	$listing = $db->get("listings","*",["id" => (int)$lid]);

	$listing["offered_brand"] = get_brand_info($listing["offered_brand"]);
	$listing["offered_brand"]["offered_honey"] = $listing["offered_honey"];
	unset($listing["offered_honey"]);

	$listing["requested_brand"] = get_brand_info($listing["requested_brand"]);
	$listing["requested_brand"]["requested_honey"] = $listing["requested_honey"];
	unset($listing["requested_honey"]);

	$listing["lister_info"] = get_user_data($listing["user_id"]);

	$output["listing"] = $listing;
	write($output,false,200);

});

$app->put("/listing/:lid", "authenticate",function($lid) use($app){

	global $db;
	global $user_id;

	$listing = $db->get("listings","*",["id" => (int)$lid]);

	if(valid_parameters("offered_brand,offered_honey,requested_brand,requested_honey","PUT"))
	{
		$sender_id = $listing["user_id"];
		$offered_brand = $app->request->put("offered_brand");
		$offered_honey = $app->request->put("offered_honey");
		$requested_brand = $app->request->put("requested_brand");
		$requested_honey = $app->request->put("requested_honey");

		make_offer($user_id,$offered_brand,$offered_honey,$sender_id,$requested_brand,$requested_honey,$lid);
	}

});

//Like accept it [Trade it now]
$app->patch("/listing/:lid", "authenticate",function($lid) use($app){

	global $db;
	global $user_id;

	$listing = $db->get("listings","*",["id" => (int)$lid]);

	$seller_id = $listing["user_id"];
	$offered_honey = $listing["offered_honey"];
	$offered_brand = $listing["offered_brand"];
	$requested_brand = $listing["requested_brand"];
	$requested_honey = $listing["requested_honey"];

	if(has_honey($user_id,$requested_brand,$requested_honey))
	{
		//Archive Listing
		$db->update("listings",["is_hidden" => 1],["id" => $lid]);

		unlock_honey($seller_id,$offered_brand,$offered_honey,1,$lid);
		if(trade($seller_id,$offered_brand,$offered_honey,$user_id,$requested_brand,$requested_honey))
		{
			setNotification($seller_id,4,$lid,"Listed Trade Completed");
			write("Trade Executed Successfully",false,200);
		}
		else
			write("Error",true,200);
	}

	


});

$app->delete("/listing/:lid", "authenticate",function($lid) use($app){

	global $db;

	//Check if Listing Owner
	if(owns_listing($lid))
	{
		$db->update("listings",["is_hidden"=>1],["id" => $lid]);

		if(!is_db_error())
		{	
			$listing = $db->get("listings",["user_id","offered_honey","offered_brand"],["id" => $lid]);
			unlock_honey($listing["user_id"],$listing["offered_brand"],$listing["offered_honey"],1,$lid);

			write("Archived Successfully",false,200);
		}
		else
			write("DB Error",true,200);
	}

});

