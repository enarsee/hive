<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

function upload($dir,$file_name)
{
$target_dir = "images/".$dir."/";
$imageFileType = pathinfo($_FILES["image"]["name"],PATHINFO_EXTENSION);
$target_file = $target_dir . basename($file_name.".".$imageFileType);
$uploadOk = 1;
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check !== false) {
       # echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
      #  echo "File is not an image.";
        $uploadOk = 0;
    }
}

// Check if file already exists
if (file_exists($target_file)) {
 #   echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["image"]["size"] > 500000) {
  #  echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    //Error
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

return $target_file;
}

//Login END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/login/
Allowed : POST
Parameters Requried: email, password

Returned Output:

{
    "user": {
        "id": "36",
        "email": "enarsee@gmail.com",
        "api_key": "ddbbf2f1d6c1ecc",
        "expiry_time": "2014-12-04 04:53:30",
        "links": {
            "self": "/user/36",
            "brands": "/user/36/brands",
            "favourites": "/user/36/favourites"
        }
    },
    "error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->post('/test/', function() use($app)
{
    upload("user","2");
	print_r($_FILES);
});