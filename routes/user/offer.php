<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/user/:id/offers", "authenticate",function($id) use($app){

	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["OR" => ["sender_id" => $user_id,
		"receiver_id" => $user_id]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["receiver"] = get_user_data($offer["receiver_id"]);

		$offers[$key]["sender"] = get_user_data($offer["sender_id"]);

		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);


});

$app->get("/user/:id/offers/sent","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["sender_id" => $user_id]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["receiver"] = get_user_data($offer["receiver_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/sent/pending","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["sender_id" => $user_id,
		"status" => 0]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["receiver"] = get_user_data($offer["receiver_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/sent/accepted","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["sender_id" => $user_id,
		"status" => 1]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["receiver"] = get_user_data($offer["receiver_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/sent/countered","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["sender_id" => $user_id,
		"status" => 3]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["receiver"] = get_user_data($offer["receiver_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});


$app->get("/user/:id/offers/sent/cancelled","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["sender_id" => $user_id,
		"status" => [2,4]]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["receiver"] = get_user_data($offer["receiver_id"]);

		if($offer["status"] == 2)
			$offers[$key]["cancelled_by"] = get_user_data($offer["receiver_id"]);
		else
			$offers[$key]["cancelled_by"] = get_user_data($offer["sender_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/received","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["receiver_id" => $user_id]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["sender"] = get_user_data($offer["sender_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/received/pending","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["receiver_id" => $user_id,
		"status" => 0]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["sender"] = get_user_data($offer["sender_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/received/accepted","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["receiver_id" => $user_id,
		"status" => 1]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["sender"] = get_user_data($offer["sender_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/received/countered","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["receiver_id" => $user_id,
		"status" => 3]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["sender"] = get_user_data($offer["sender_id"]);


		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});

$app->get("/user/:id/offers/received/cancelled","authenticate",function($id) use ($app)
{
	global $db;
	global $user_id;
	$user_id = (int)$user_id;

	is_owner($id);

	
	$offers = $db->select("offers","*",["AND" => ["receiver_id" => $user_id,
		"status" => [2,4]]]);


	foreach($offers as $key => $offer)
	{
		

		$offers[$key]["offered_brand"] = get_user_brand_data($offer["offered_brand"]);


		$offers[$key]["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

		$offers[$key]["sender"] = get_user_data($offer["sender_id"]);

		if($offer["status"] == 2)
			$offers[$key]["cancelled_by"] = get_user_data($offer["receiver_id"]);
		else
			$offers[$key]["cancelled_by"] = get_user_data($offer["sender_id"]);

		unset($offers[$key]["sender_id"]);
		unset($offers[$key]["receiver_id"]);		

		
	}


	$output["offers"] = $offers;

	$output["meta"]["total"] = count($offers);

	write($output,false,200);
});


$app->post("/user/:id/offers", "authenticate",function($id) use($app){

	global $db;
	global $user_id;

	if(valid_parameters("offered_brand,offered_honey,receiver_id,requested_brand,requested_honey","POST"))
	{
		$offered_brand = $app->request->post("offered_brand");
		$offered_honey = $app->request->post("offered_honey");
		$receiver_id = $app->request->post("receiver_id");
		$requested_brand = $app->request->post("requested_brand");
		$requested_honey = $app->request->post("requested_honey");

		make_offer($user_id,$offered_brand,$offered_honey,$receiver_id,$requested_brand,$requested_honey);
	}


});


$app->get("/user/:id/offer/:oid", "authenticate",function($id,$oid) use($app){

global $db;

is_owner($id);

if(has_offer($oid))
{
	$offer = $db->get("offers","*",["id" => $oid]);

	$output = array();

	$offer["sender"] = get_user_data($offer["sender_id"]);
	$offer["receiver"] = get_user_data($offer["receiver_id"]);
	$offer["offered_brand"] = get_user_brand_data($offer["offered_brand"]);
	$offer["requested_brand"] = get_user_brand_data($offer["requested_brand"]);

	unset($offer["sender_id"]);
	unset($offer["receiver_id"]);

	$output["offer"] = $offer;


	write($output,false,200);
}
else
{
	write("Unauthorized",true,200);
}



});

$app->delete("/user/:id/offer/:oid", "authenticate",function($id,$oid) use($app){

global $db;

is_owner($id);
$offer = $db->get("offers",["id","sender_id","receiver_id","offered_honey","offered_brand","requested_honey","requested_honey"],["id" => $oid]);

if(owns_offer($oid))
{
	$db->update("offers",["status" => 4],["id" => $oid]);
	if(!is_db_error())
		{
			unlock_honey($offer["sender_id"],$offer["offered_brand"],$offer["offered_honey"],0,$oid);
			write("Offer Withdrawn Successfully",false,200);
		}
}
else if(has_valid_offer($oid))
{
	$db->update("offers",["status" => 2],["id" => $oid]);

	if(!is_db_error())
	{
		$offer = $db->get("offers",["id","sender_id","receiver_id","offered_honey","offered_brand","requested_honey","requested_honey"],["id" => $oid]);

		if(!empty($offer))
		{

			unlock_honey((int)$offer["sender_id"],(int)$offer["offered_brand"],(float)$offer["offered_honey"],0,$oid);

			setNotification($offer["sender_id"],"offer_rejected",$oid,"Your offer has been rejected !");
			write("Offer Rejected Successfully",false,200);
		}
	}
}
else
	write("Unauthorized",true,200);

});


//Accept
$app->patch("/user/:id/offer/:oid","authenticate", function($id,$oid) use($app)
{
	global $db;

	is_owner($id);


	if(has_valid_offer($oid))
	{
	
	$offer = $db->get("offers",["id","sender_id","receiver_id","offered_honey","offered_brand","requested_brand","requested_honey","from_listing"],["id" => $oid]);

	$db->update("offers",["status" => 1],["id" => $oid]);

	if(!is_db_error())
	{
		$sender_id = $offer["sender_id"];
		$receiver_id = $offer["receiver_id"];
		$offered_brand = $offer["offered_brand"];
		$offered_honey = $offer["offered_honey"];
		$requested_brand = $offer["requested_brand"];
		$requested_honey = $offer["requested_honey"];
		$listing_id = $offer["from_listing"];



		if(has_honey($receiver_id,$requested_brand,$requested_honey))
		{
			if($listing_id!=0)
			{
				$listing = $db->get("listings",["user_id","offered_brand","offered_honey"],["id" => $listing_id]);
				unlock_honey($listing["user_id"],$listing["offered_brand"],$listing["offered_honey"],0,$oid);
				$db->update("listings",["is_hidden" => 1],["id" => $listing_id]);
			}

			unlock_honey($sender_id,$offered_brand,$offered_honey,0,$oid);
			if(trade($sender_id,$offered_brand,$offered_honey,$receiver_id,$requested_brand,$requested_honey))
			{
				setNotification($sender_id,"offer_accepted",$oid,"Your offer has been accepted !");
				write("Trade Successful",true,200);
			}
			else
				write("Error Occured",true,200);
		}


	}
	}
	else
		write("Unauthorized",true,200);
});


//Counter
$app->put("/user/:id/offer/:oid", "authenticate",function($id,$oid) use($app){

global $db;

is_owner($id);

if(has_valid_offer($oid))
{
	if(valid_parameters("offered_honey,offered_brand,requested_honey,requested_brand","PUT"))
	{
		$offer = $db->get("offers",["sender_id","offered_brand","offered_honey","receiver_id","requested_brand","requested_honey","from_listing"],[
			"id" => $oid]);

		unlock_honey($offer["sender_id"],$offer["offered_brand"],$offer["offered_honey"],0,$oid);

		$db->update("offers",["status" => 3],["id" => $oid]);

		setNotification($offer["sender_id"],"offer_countered",$oid,"You have a counter offer !");

		//Now make new offer
		$sender_id = $offer["receiver_id"];
		$receiver_id = $offer["sender_id"];
		$listing_id = $offer["from_listing"];
		$offered_honey = $app->request->put("offered_honey");
		$offered_brand = $app->request->put("offered_brand");
		$requested_brand = $app->request->put("requested_brand");
		$requested_honey = $app->request->put("requested_honey");

		make_offer($sender_id,$offered_brand,$offered_honey,$receiver_id,$requested_brand,$requested_honey,$listing_id);
		
	}
}
else
	write("Unauthorized",true,200);


});
