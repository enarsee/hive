<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/user/:id/messages", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "OR" => [
		"sender_id" => $id,
		"receiver_id" => $id]]);

	$m_array = array();

	foreach($messages as $message)
	{
		$check_id;
		if($id == $message["sender_id"])
			$check_id = $message["receiver_id"];
		else
			$check_id = $message["sender_id"];

		$message["sender"] = get_user_data($message["sender_id"]);
		$message["receiver"] = get_user_data($message["receiver_id"]);

		unset($message["sender_id"]);
		unset($message["receiver_id"]);

		if(array_key_exists($check_id, $m_array))
		{
			array_push($m_array[$check_id]["messages"],$message);
			$m_array[$check_id]["meta"]["total"] += 1;
		}
		else
		{
			$m_array[$check_id] = array();
			$m_array[$check_id]["user_info"] = get_user_data($check_id);
			$m_array[$check_id]["messages"] = array();
			$m_array[$check_id]["meta"]["total"]  = 1;
			array_push($m_array[$check_id]["messages"],$message);
		}
	}

	$output["messages"] = array();

	foreach($m_array as $key=>$element)
		array_push($output["messages"],$element);

	$output["meta"]["total"] = count($messages);	
	write($output,false,200);

});



/* 

Depreciated

$app->get("/user/:id/messages", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "OR" => [
		"sender_id" => $id,
		"receiver_id" => $id]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

*/


$app->get("/user/:id/messages/read", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "AND" => ["is_read"=> 1, "OR" => [
		"sender_id" => $id,
		"receiver_id" => $id]]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/unread", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "AND" => ["is_read"=> 0, "OR" => [
		"sender_id" => $id,
		"receiver_id" => $id]]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/sent", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "sender_id" => $id]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/sent/read", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "AND" => ["sender_id" => $id,
		"is_read" => 1]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/sent/unread", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "AND" => ["sender_id" => $id,
		"is_read" => 0]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/received", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "receiver_id" => $id]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/received/read", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "AND" => ["receiver_id" => $id,
		"is_read"=> 1]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/received/unread", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		[ "AND" => ["receiver_id" => $id,
		"is_read"=> 0]]);

	$output = array();

	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);


		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);

});

$app->get("/user/:id/messages/:sid","authenticate", function($id,$sid) use($app)
{
	global $db;
	is_owner($id);

	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		["OR #Or" => ["AND #ANd1" => ["sender_id" => $id,
		"receiver_id" => $sid],"AND #And2" => ["sender_id" => $sid,
		"receiver_id" => $id]]]);
	$output = array();


	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);

		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);


});

$app->get("/user/:id/messages/:sid/read","authenticate", function($id,$sid) use($app)
{
	global $db;
	is_owner($id);

	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		["AND #a1" => [
			"OR #Or" => [
				"AND #ANd1" => [
					"sender_id" => $id,"receiver_id" => $sid
								]
				,"AND #And2" => [
					"sender_id" => $sid,"receiver_id" => $id
								]
						]
					, "is_read" => 1
					]]);
	$output = array();


	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);

		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);


});

$app->get("/user/:id/messages/:sid/unread","authenticate", function($id,$sid) use($app)
{
	global $db;
	is_owner($id);

	$messages = $db->select("messages",[
		"id","sender_id","receiver_id","message","is_read","sent_date"],
		["AND #a1" => [
			"OR #Or" => [
				"AND #ANd1" => [
					"sender_id" => $id,"receiver_id" => $sid
								]
				,"AND #And2" => [
					"sender_id" => $sid,"receiver_id" => $id
								]
						]
					, "is_read" => 0
					]]);
	$output = array();


	foreach ($messages as $key=>$message)
	{
		$messages[$key]["sender"] = get_user_data($message["sender_id"]);
		unset($messages[$key]["sender_id"]);

		$messages[$key]["receiver"] = get_user_data($message["receiver_id"]);
		unset($messages[$key]["receiver_id"]);

		$messages[$key]["links"]["self"] = "/user/".$id."/message/".$message['id'];
	}
	$output["messages"] = $messages;


	
	write($output,false,200);


});

$app->get("/user/:id/message/:mid","authenticate", function($id,$mid) use ($app)
{
	global $db;

	is_owner($id);
	is_message_owner($mid);

	$message = $db->get("messages",["id","message","sender_id","receiver_id","sent_date"],[
			"id" => $mid]);
	
	$message["sender"] = get_user_data($message["sender_id"]);

	$message["receiver"] = get_user_data($message["receiver_id"]);

	

	$message["links"]["sender"] = "/user/".$message['sender_id'];
	unset($message["sender_id"]);
	$message["links"]["receiver"] = "/user/".$message["receiver_id"];
	unset($message["receiver_id"]);

	write($message,false,200);

});

$app->delete("/user/:id/message/:mid","authenticate", function($id,$mid) use ($app)
{
	global $db;

	is_owner($id);
	is_message_owner($mid);

	$message = $db->delete("messages",[
			"id" => $mid]);
	
	write("Deleted",false,200);

});

$app->patch("/user/:id/message/:mid","authenticate", function($id,$mid) use ($app)
{
	global $db;

	is_owner($id);
	is_message_owner($mid);

	$message = $db->update("messages",["is_read" => 1],[
			"id" => $mid]);
	
	write("Marked as read",false,200);

});

$app->post("/user/:id/messages", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	if(valid_parameters("receiver_id,message","POST"))
	{
	$receiver_id = $app->request->post("receiver_id");
	$message = $app->request->post("message");

	$sent = $db->insert("messages",[
		"sender_id" => $id,
		"receiver_id" => $receiver_id,
		"message" => $message,
		"is_read" => 0,
		"#sent_date" => "NOW()"
		]);

	if($db->error()[2]=="")
	{
		$message = $db->get("messages",["id","message","sender_id","receiver_id","sent_date"],[
			"id" => $sent]);

		$message["sender"] = get_user_data($message["sender_id"]);
		unset($message["sender_id"]);
		$message["receiver"] = get_user_data($message["receiver_id"]);
		unset($message["receiver_id"]);

		$message["links"]["self"] = "/user/".$id."/message/".$sent;

		setNotification($receiver_id,"message_received",$sent,"You have a new message!");

		write($message,false,201);
	}
	else
	{
		write($db->error()[2],true,200);
	}

	}
	

});