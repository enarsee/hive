<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/user/:id/notifications", "authenticate",function($id) use($app){

	global $db;
	global $user_id;
	is_owner($id);

	if(isset($_GET['filter']))
		$filter = 0;
	else
		$filter = [0,1];

	$notifications = $db->select("notifications",["id","sender_id","type_id","link_id","message","is_seen","created_on"],["AND" => ["user_id" => $id,
		"is_seen"=> $filter]]);

	$output["notifications"] = $notifications;
	$output["meta"]["total"] = count($notifications);

	write($output,false,200);

});

$app->delete("/user/:id/notification/:nid", "authenticate",function($id,$nid) use($app){

	global $db;
	global $user_id;
	is_owner($id);

	is_notif_owner($nid);

	$notifications = $db->update("notifications",["is_seen"=> 1],["id" => $nid]);



	write("Read Successfully",false,200);

});

 