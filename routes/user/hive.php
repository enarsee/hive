<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING


//Get List of all hives
$app->get("/user/:id/hives","authenticate", function($id) use($app){
	global $user_id;
	
	is_owner($id);

	if(!(isset($_GET['filter'])))
		$filter = 0;
	else	
		$filter = $_GET['filter'];

	//0 -> All ; 1 -> Hives I am a part of ; 2 -> Hive's I have requested ; 3 -> Hive's I have been invited to ; 4 -> Hives I can join 

	global $db;

switch($filter){
	case 0:
		$hives = $db->select("hives",["id","owner_id","created_on"]);
		foreach($hives as $key=>$hive)
		{
			$owner_id = $hive["owner_id"];
			unset($hives[$key]["owner_id"]);
			$hives[$key]["owner"] = $db->get("users",["id","first_name","last_name","city","state","country","photo"],["id" => $owner_id]);
			global $server_path;
			$hives[$key]["owner"]["photo"] = $server_path.$hives[$key]["owner"]["photo"];
			$hives[$key]["links"]["self"] = "/user/".$id."/hive/".$hive["id"];
		}
		#$hives = $db->select("hives", ["[><]users" => ["hives.owner_id" => "users.id"]],["hives.id","name","description","photo","owner_id","created_on"]);
		break;
	case 1: case 2: case 3:

		$hives = $db->select("hive_members", ["[><]hives" => ["hive_id" => "id"]], ["hive_id","owner_id","created_on"],["AND" => [ "user_id" => $id, "status" => $filter]]);
		foreach($hives as $key=>$hive)
		{
			$owner_id = $hive["owner_id"];
			unset($hives[$key]["owner_id"]);
			$hives[$key]["owner"] = $db->get("users",["id","first_name","last_name","city","state","country","photo"],["id" => $owner_id]);
			global $server_path;

					$hives[$key]["owner"]["photo"] = $server_path.$hives[$key]["owner"]["photo"];
			$hives[$key]["links"]["self"] = "/user/".$id."/hive/".$hive["hive_id"];
		}

		break;
	case 4:
		$hives = array();
		$hives_all = $db->select("hives", ["id","owner_id","created_on"]);
		$hives_in = $db->select("hive_members", ["hive_id"],["AND" => [ "user_id" => $id, "status" => [0,1,2]]]);
		foreach($hives_all as $hive)
		{
			if(!(in_array_multi($hives_in,"hive_id",$hive["id"])))
				{
					$owner_id = $hive["owner_id"];
					unset($hive["owner_id"]);
					$hive["owner"] = $db->get("users",["id","first_name","last_name","city","state","country","photo"],["id" => $owner_id]);
					global $server_path;

					$hive["owner"]["photo"] = $server_path.$hive["owner"]["photo"];
					$hive["links"]["self"] = "/user/".$id."/hive/".$hive["id"];
					array_push($hives,$hive);
				}
				
		}
		break;
}
		$output["hives"] = $hives;
		$output["meta"]["total"] = count($hives);

	write($output,false,200);
});

//Create a Hive
$app->post("/user/:id/hives","authenticate",function($id) use($app){
	global $user_id;
	global $db;
	is_owner($id);

	$hives = $db->select("hives", ["id","owner_id"], ["owner_id" => $id]);

	if(count($hives) > 0)
	{
		write("Hive already exists",true,200);
	}


	$db->insert("hives",["owner_id" => $id,
		"#created_on" => "NOW()"]);

	$hive = $db->get("hives",["id"],["owner_id" => $id]);
	$hid = $hive["id"];
	$db->insert("hive_members",["hive_id"=>$hid,
		"user_id"=>$id,
		"status" => 0,
		"#created_date"=>"NOW()"]);

	write("Successfully created",false,200);

});

//View a Hive's details
$app->get("/user/:id/hive/:hid","authenticate",function($id,$hid) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);



	//is_hive_owner($hid);

	if($hid == "own")
	{
		$my_hive = $db->get("hives",["id","owner_id"],["owner_id" => $id]);
		if(!(empty($my_hive)))
			$hid = $my_hive["id"];
		else
			write("Please create your own hive first",true,200);
	}
	
	$hive = $db->get("hives",["id","owner_id","created_on"],["id"=>$hid]);

	$owner = $db->get("users",["id","first_name","last_name","city","state","country","photo"],["id" => $hive["owner_id"]]);

	

	unset($hive["owner_id"]);
	$hive["owner"] = $owner;
	$output["info"] = $hive;



	$hive_in = $db->select("hive_members",["hive_id","user_id"],["hive_id" => $hid]);



	$brand = array();

	

	$contribution = array();

	foreach($hive_in as $hive)
	{

		//Get the user
		$user = $hive["user_id"];

		//Go through their brands
		$honey = $db->select("honey",["brand_id","honey"],["user_id" => $user]);

		$contribution[$user]["brands"] = array();
		//Add it up
		foreach($honey as $h)
		{


			if(isset($brand[$h["brand_id"]]))
				$brand[$h["brand_id"]] = (float)$brand[$h["brand_id"]] + (float)$h["honey"];
			else
				$brand[$h["brand_id"]] = (float)$h["honey"]; 

			//Get Locked Honey
			$locked = get_locked_honey($user,$h["brand_id"]);

			$b = array();
			$b["info"] = get_brand_info((int)$h["brand_id"]);
			$b["honey"] = (float)$h["honey"];
			$b["locked"] = $locked;

			array_push($contribution[$user]["brands"], $b);
			

			$brand[$h["brand_id"]] = (float)$brand[$h["brand_id"]] + (float)$locked;
		}
	}

	$brands = array();

	foreach($brand as $k=>$b)
	{

		/*$temp = array();
		$temp["brand_id"] = $k;
		$temp["honey"] = $b;
		$temp["level_info"] = getLevel($b,$k);*/
		//array_push($brands,$temp);
		$temp = array();
		$temp["info"] = get_brand_info($k);
		$temp["hive"] = get_brand_data($k,$b,false);
		
		$temp["user"] = get_user_brand_data($k,false);


		array_push($brands,$temp);
	}

	$output["brands"] = $brands;

	$members = $db->select("hive_members", ["[><]users" => ["user_id" => "id"]], ["users.id","first_name","last_name","photo"], ["AND" => ["hive_id" => $hid,
		"status" => 1]]);

	foreach($members as $key=>$member)
	{
		global $server_path;

		$members[$key]["photo"] = $server_path.$member["photo"];
		$members[$key]["contribution"] = $contribution[$member["id"]];
	}


	$mem= array();
	$mem["members"] = $members;
	$mem["meta"]["total"] = count($members);


	$output["users"] = $mem;

	$output["links"]["users"] = "/user/".$id."/hive/".$hid."/users";

	write($output,true,200);

});



//UPdate YOUR Hive's details
/*
$app->put("/user/:id/hive/:hid","authenticate",function($id,$hid) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);
	//is_hive_owner($hid);

	
		if(is_hive_owner($id,$hid))
		{
			$name = $app->request->get("name");
			$description = $app->request->get("description");
			$photo = $app->request->get("photo");

			$db->update("hives",[
				"name" => $name,
				"description" => $description,
				"photo" => $photo],["id"=>$hid]);

			write("Hive Updated Successfully",false,200);

		}
		else
			write("Unauthorized",true,200);
	
	
});
*/

//Delete YOUR Hive
$app->delete("/user/:id/hive/:hid","authenticate",function($id,$hid) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);
	//is_hive_owner($hid);
	if($hid == "own")
	{
		$my_hive = $db->get("hives",["id","owner_id"],["owner_id" => $id]);
		if(!(empty($my_hive)))
			$hid = $my_hive["id"];
		else
			write("Please create your own hive first",true,200);
	}

	
		if(is_hive_owner($id,$hid))
		{

			$db->delete("hives",["id"=>$hid]);

			write("Hive Deleted Successfully",false,200);

		}
		else
			write("Unauthorized",true,200);
	
	
});

//View a Hive's users
$app->get("/user/:id/hive/:hid/users","authenticate", function($id,$hid) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	if($hid == "own")
	{
		$my_hive = $db->get("hives",["id","owner_id"],["owner_id" => $id]);
		if(!(empty($my_hive)))
			$hid = $my_hive["id"];
		else
			write("Please create your own hive first",true,200);
	}

	$members = $db->select("hive_members", ["[><]users" => ["user_id" => "id"]], ["users.id","first_name","last_name","photo"], ["AND" => ["hive_id" => $hid,
		"status" => [0,1]]]);

	

	$output= array();
	$output["members"] = $members;
	$output["meta"]["total"] = count($members);

	//TBD
	//Links in each member

	write($output,false,200);

});

//Delete a specific Hive User
$app->delete("/user/:id/hive/:hid/user/:uid","authenticate", function($id,$hid,$uid) use($app){
	global $user_id;
	global $db;
	is_owner($id);

	if($hid == "own")
	{
		$my_hive = $db->get("hives",["id","owner_id"],["owner_id" => $id]);
		if(!(empty($my_hive)))
			$hid = $my_hive["id"];
		else
			write("Please create your own hive first",true,200);
	}


	if(is_hive_owner($id,$hid))
	{
		$db->delete("hive_members",["AND" => [
			"hive_id" => $hid,
			"user_id" => $uid]]);
		write("Deleted Successfully",false,200);
	}
	else
	{
		if($id == $uid)
		{
			if(is_hive_member($id,$hid,1))
			{
				$db->delete("hive_members",["AND" => [
				"hive_id" => $hid,
				"user_id" => $id]]);
				
		write("Deleted Successfully",false,200);
			}
		}
	else
		write("Unauthorized",true,200);
	}
});


//Invite/Request joining a Hive
$app->post("/user/:id/hive/:hid/users","authenticate",function($id,$hid) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	if($hid == "own")
	{
		$my_hive = $db->get("hives",["id","owner_id"],["owner_id" => $id]);
		if(!(empty($my_hive)))
			$hid = $my_hive["id"];
		else
			write("Please create your own hive first",true,200);
	}

	if(is_hive_owner($id,$hid))
	{
		
		if(isset($_POST['invite_id']))
			{
				$invite_id = $app->request->post("invite_id");

				//Check if invited
				$hive_users = $db->select("hive_members",["user_id"],["hive_id" => $hid]);
				foreach($hive_users as $user)
				{
					if($invite_id == $user["user_id"])
					{
						write("Invalid Action (Exists!)",true,200);
					}
				}

				//Invite people
				$db->insert("hive_members",["hive_id"=>$hid,
					"user_id"=>$invite_id,
					"status"=>3,
					"#created_date" => "NOW()"]);

				setNotification($invite_id,"hive_invited",$hid,"You have been invited to join a Hive !");

			write("Invited Successfully",false,200);
		}
		else
			write("Error",true,200);

		//Error handling
	}
	else
		write("Only the owner can add a new user",true,200);

	/*
	else if(is_hive_member($id,$hid,1))
	{
		write("You are already a member",true,200);
	}
	else if(is_hive_member($id,$hid,2))
		write("You have already sent a request",true,200);
	else if(is_hive_member($id,$hid,3))
		write("You have been invited to this Hive",true,200);
	else
	{
		//Put in a request
		$result = $db->insert("hive_members",["hive_id"=>$hid,
			"user_id"=>$id,
			"status"=>2,
			"#created_date" => "NOW()"]);

		$hive_data = $db->get("hives",["owner_id"],["id" => $hid]);

		setNotification($hive_data["owner_id"],2,$hid,"Someone has requested to join your hive");

		write("Requested Successfully",false,200);
	}
	*/
	//Add to hive


});


//Moderate requests

$app->get("/user/:id/hive/:hid/requests","authenticate",function($id,$hid) use ($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	

	if(is_hive_owner($id,$hid))
	{
		$wannabe = $db->select("hive_members",["[><]users" => ["user_id"=> "id"]],["hive_members.id","users.id","first_name","last_name","photo","status","created_date"],
			["AND" => ["hive_id" => $hid,
			"status" => 2]]);
		
		$output = array();

		$output["requests"] = $wannabe;
		$output["meta"]["total"] = count($wannabe);
		write($output,false,200);
	}
	else
		write("Unauthorized",true,200);
});


$app->get("/user/:id/hive/:hid/request/:uid","authenticate",function($id,$hid,$uid) use ($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	

	if(is_hive_owner($id,$hid))
	{
		$requester = $db->select("hive_members",["[><]users" => ["user_id"=> "id"]],["hive_members.id","users.id","first_name","last_name","photo","status","created_date"],
			["AND" => ["users.id" => $uid,
			"hive_id" => $hid]]);

		$output = array();
		$output["user"] = $requester;
		write($output,false,200);
	}
	else
		write("Unauthorized",true,200);
});


$app->put("/user/:id/hive/:hid/request/:uid","authenticate", function($id,$hid,$uid ) use ($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	if(is_hive_owner($id,$hid))
	{
		$db->update("hive_members",["status" => 1], ["AND"=>
			["hive_id" => $hid,
			"user_id" => $uid]]);

		setNotification($uid,"hive_accepted",$hid,"Your Hive Request has been accepted");


	}
	else
		write("Unauthorized",true,200);
});

$app->delete("/user/:id/hive/:hid/request/:uid","authenticate", function($id,$hid,$uid ) use ($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	if(is_hive_owner($id,$hid))
	{
		$db->delete("hive_members", ["AND"=>
			["hive_id" => $hid,
			"user_id" => $uid]]);

		setNotification($uid,"hive_rejected",$hid,"Your Hive Request has been rejected");

	}
	else
		write("Unauthorized",true,200);
});

$app->get("/user/:id/hives/invitations","authenticate", function($id) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	$invitations = $db->select("hive_members",["[><]hives" => ["hive_id" => "id"]], ["hives.id","hives.owner_id","hives.created_on"],[
		"AND" => ["user_id" => $user_id,
		"status" => 3]]);


	foreach($invitations as $key=>$hive)
		{
			$owner_id = $hive["owner_id"];
			unset($invitations[$key]["owner_id"]);
			$invitations[$key]["owner"] = $db->get("users",["id","first_name","last_name","city","state","country","photo"],["id" => $owner_id]);
			$invitations[$key]["links"]["self"] = "/user/".$id."/hive/".$hive["id"];
		}
	$output =array();

	$output["invitations"] = $invitations;
	$output["meta"]["total"] = count($invitations);

	write($output,false,200);
});

$app->put("/user/:id/hives/invitation/:hid","authenticate",function($id,$hid) use($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	if(is_hive_member($id,$hid,3))
	{
		$db->update("hive_members",["status" => 1], ["AND"=>
			["hive_id" => $hid,
			"user_id" => $id]]);

		$hive = $db->get("hives",["owner_id"],["id" => $hid]);

		setNotification($hive["owner_id"],"hive_accepted",$hid,"Your Hive Invitation has been accepted");


	write("Accepted Successfuly",false,200);

	}
	else
		write("Unauthorized",true,200);
});

$app->delete("/user/:id/hives/invitation/:hid","authenticate",function($id,$hid) use ($app)
{
	global $user_id;
	global $db;
	is_owner($id);

	if(is_hive_member($id,$hid,3))
	{
	$db->delete("hive_members", ["AND"=>
			["hive_id" => $hid,
			"user_id" => $id]]);

	$hive = $db->get("hives",["owner_id"],["id" => $hid]);

	setNotification($hive["owner_id"],"hive_rejected",$hid,"Your Hive Invitation has been rejected");

	write("Deleted Successfuly",false,200);

	}
	else
		write("Unauthorized",true,200);
});