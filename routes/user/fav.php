<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/user/:id/favourites", "authenticate",function($id) use($app){

	global $db;

	$data = $db->select("favourites",[
		"brand_id"],[
		"user_id" => $id]);
	$output = array();

	$output["favourites"] = array();
	$output["meta"] = array();
	$total = 0;
	$count = count($data);

	foreach($data as $item)
	{
		#$links["brand"] = "/brand/".$item['brand_id'];
		$links["honey"] = "/user/".$id."/brand/".$item['brand_id'];
		$item["embedded"]["brand"] = getBrandData($item['brand_id']);

		//Get Honey Info
		$honey = $db->get("honey",["honey"],["AND" => ["user_id" => $id,
			"brand_id" => $item["brand_id"]]]);

		if(!empty($honey))
		{
			$item["embedded"]["brand"]["honey"] = (float)($honey["honey"]);
			$item["embedded"]["brand"]["level_info"] = getLevel($honey['honey'],$item['brand_id']);
		}
		else
		{
			$item["embedded"]["brand"]["honey"] = false;
			$item["embedded"]["brand"]["level_info"] = false;
		}



		$item["links"] = $links;
		array_push($output["favourites"],$item);
	}


	$output["meta"]["total_favourites"] = $count;


	write($output,false,200);

});

//Additional GET for getting brands which can be added
$app->get("/user/:id/favourites/add/","authenticate",function($id)use($app){
	is_owner($id);

	global $db;

	$brands = $db->select("brands",[
		"id",
		"name",
		"description",
		"photo",
		"created_on"
		]);

	$user_brands = $db->select("favourites",[
		"brand_id"],[
		"user_id" => $id]);

	$output["brands"] = array();

	foreach ($brands as $brand)
	{
		if(!(in_array_multi($user_brands,"brand_id",$brand['id'])))
		{
			array_push($output["brands"], $brand);
		}
	}

	write($output,false,200);
});

$app->post("/user/:id/favourites","authenticate",function($id)use($app){

	is_owner($id);

	if(valid_parameters("brand_id","POST"))
	{
		global $db;

		$brand_id = $app->request->post("brand_id");

		$rows = $db->insert("favourites",[
			"user_id" => $id,
			"brand_id" => $brand_id]);

		if($db->error()[2]=="")
		{
			write("Success",false,201);
		}
		else
		{
			write($db->error()[2],true,400);
		}
	}
});

$app->delete("/user/:id/favourite/:bid","authenticate",function($id,$bid) use($app){

	is_owner($id);

	global $db;

	$db->delete("favourites",["AND" =>
		["user_id" => $id,
		"brand_id" => $bid]]);

	write("Delete Successful",false,200);

});

$app->get("/user/:id/favourites/common","authenticate",function($id) use($app){
	global $db;

	is_owner($id);

	$favs = $db->select("favourites","*");

	$fav_user= array();
	$fav_mine = array();

	$my_favs = $db->select("favourites",["brand_id"],["user_id" => $id]);
	foreach($my_favs as $fav)
	{
		array_push($fav_mine,$fav["brand_id"]);
	}


	foreach($favs as $row)
	{
		if(!array_key_exists($row["user_id"], $fav_user))
		{
				$fav_user[$row["user_id"]] = 0;
		}
		$fav_user[$row["user_id"]] = $fav_user[$row["user_id"]] + 1;
	}
	unset($fav_user[$id]);
	arsort($fav_user);

	$output["common_favourites"] = array();

	foreach($fav_user as $key=>$row)
	{
		$user["info"] = $db->get("users",["first_name","last_name","photo","city","state","country","gender"],["id" => (int)$key]);
	
		$user["common_favourites"] = $row;
		$user["links"]["self"] = "/user/".$key;
		array_push($output["common_favourites"],$user);

	}

	$output["meta"]["total"] = count($output["common_favourites"]);

	write($output,false,200);




});