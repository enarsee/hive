<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING
$app->get("/users/", "authenticate", function() use($app){

global $db;


$users = $db->select("users",[
	"id", "first_name", "last_name", "email", "photo", "city", "state", "country", "zip_code", "gender", "hive_privacy", "own_privacy", "created_on"]);


$output = array();
$output['users'] = array();


global $server_path;
foreach($users as $user)
{
if($user["photo"]=="")
	$user["photo"] = "/images/default.jpg";

	$user["photo"] = $server_path.$user["photo"];
	$user['links'] = getLinks($user['id'],"user");
	array_push($output['users'],$user);
}

$output['meta'] = array(
	"total" => count($output['users']));

write($output,false,200);


});







$app->post("/users/", function() use($app)
{
	write("Use /register/ to create a user", true,400);
});

$app->get("/user/:id/reactivate",function($id) use($app)
{
	global $db;
	$db->update("users",["is_active" => 1],["id" => $id]);

	write("Reactivated",false,200);
});

$app->get("/user/:id/invitable_users", function($id) use($app)
{
	global $db;


$users = $db->select("users",[
	"id", "first_name", "last_name", "email", "photo", "city", "state", "country", "zip_code", "gender", "hive_privacy", "own_privacy", "created_on"]);


$output = array();
$output['users'] = array();


global $server_path;


//Filter
//Get Hive id
$hive_id = $db->get("hives",["id"],["owner_id"=>$id]);



if(!empty($hive_id))
{
	$hive_id = $hive_id["id"];
	//Get users for that Hive
	$hive_users = $db->select("hive_members",["user_id"],["hive_id"=>$hive_id]);


	//Filter $users
	foreach($users as $user)
	{
		if(!in_array_multi($hive_users,"user_id",$user["id"]))
		{


	if($user["photo"]=="")
		$user["photo"] = "/images/default.jpg";

		$user["photo"] = $server_path.$user["photo"];
		$user['links'] = getLinks($user['id'],"user");
		array_push($output['users'],$user);
	}
	}
}

$output['meta'] = array(
	"total" => count($output['users']));

write($output,false,200);
});

$app->get("/user/:id","authenticate", function ($id) use($app)
{

	global $db;
	$users = $db->select("users",[
	"id", "first_name", "last_name", "email", "photo", "city", "state", "country", "zip_code", "gender", "birth_date", "hive_privacy","own_privacy","created_on"],["AND" => ["id" => $id,
	"is_active" => 1]]);

	$output['user'] = array();

if(count($users)>0)
{
	$user = $users[0];
	

	global $server_path;

	if($user["photo"]=="")
		$user["photo"] = "/images/default.jpg";

	$user["photo"] = $server_path.$user["photo"];
	
	


	$user["links"] = getLinks($id,"user");
	$output['user'] = $user;
	write($output,false,200);
}
else
{
	write("User Not Found",true,200);
}


});


$app->put("/user/:id","authenticate",function($id) use($app){
	global $user_id;
	global $db;

	is_owner($id);

	if(valid_parameters("first_name,last_name,email,city,state,country,zip_code,gender,birth_date,hive_privacy,own_privacy","PUT"))
	{
	

		$first_name = $app->request->put("first_name");
		$last_name = $app->request->put("last_name");
		$email = $app->request->put("email");
		$city = $app->request->put("city");
		$state = $app->request->put("state");
		$country = $app->request->put("country");
		$zip_code = $app->request->put("zip_code");
		$gender = $app->request->put("gender");
		$birth_date = $app->request->put("birth_date");
		$hive_privacy = $app->request->put("own_privacy");
		$own_privacy = $app->request->put("hive_privacy");

		if(isset($_FILES["image"]))
			{
				$photo = uploadImage("users",$id);
				$db->update("users",["photo" => $photo],["id"=>$id]);
			}


		$rows = $db->update("users",[
			"first_name" => $first_name,
			"last_name" => $last_name,
			"email" => $email,
			"city" => $city,
			"state" => $state,
			"country" => $country,
			"zip_code" => $zip_code,
			"gender" => $gender,
			"birth_date" => $birth_date,
			"hive_privacy" => $hive_privacy,
			"own_privacy" => $own_privacy,
			"#updated_on" => "NOW()"
			],[
			"id" => $id]);

		if($rows > 0)
		{
			$output = array();
			
			$user = $db->get("users",["id",
				"first_name",
				"last_name",
				"email",
				"photo",
				"city",
				"state",
				"country",
				"zip_code",
				"gender",
				"birth_date",
				"hive_privacy",
				"own_privacy"],["id" => $id]);

			global $server_path;
			$user["photo"] = $server_path.$user['photo'];

			$output["user"] = $user;
			$output["links"] = getLinks($id,"user");

			write($output,false,200);
		}
		else
		{
			write("Error",true,200);
		}
	}



});

$app->post("/user/:id","authenticate",function($id) use($app)
{
	global $user_id;
	global $db;

	is_owner($id);

	if(isset($_FILES["image"]))
			{
				$photo = uploadImage("users",$id);
				$db->update("users",["photo" => $photo],["id"=>$id]);
				write("Successfully uploaded",false,200);
			}
		else
			write("Please pass proper parameters : image",true,200);


});


$app->patch("/user/:id","authenticate",function($id) use($app){
	global $user_id;

	is_owner($id);

	global $db;


	$patch_data = $app->request->patch();
	$success = 1;

	if(array_key_exists("email", $patch_data))
	{
		if(array_key_exists("confirm_email", $patch_data))
		{
			if($patch_data["email"] != $patch_data["confirm_email"])
				write("Please check email again",true,200);
			else
				{
					unset($patch_data["confirm_email"]);
					$data = $db->get("users","*",["email"=> $patch_data["email"]]);
					if(!empty($data))
						write("Email already exists",true,200);

				}
		}
		else
			write("Confirm email needed",true,200);

	}

	if(array_key_exists("password", $patch_data))
	{
		$headers = $app->request->headers;

		if(array_key_exists("confirm_password", $patch_data))
		{
			if($patch_data["password"] != $patch_data["confirm_password"])
				write("Please check password field again",true,200);
			else
				unset($patch_data["confirm_password"]);
		}
		
		//Check if old_password also sent 
		if(array_key_exists("old_password",$patch_data))
		{
			//Verify
			$user = $db->get("users",["email"],["id" => $id]);
			$email = $user["email"];
			if(check_login($email,$patch_data["old_password"],true))
			{
				unset($patch_data["old_password"]);
			}
			else
				write("Invalid Password",true,200);
		}
		else
		{
			if(isset($headers["ResetKey"]))
			{
				if(!check_reset_key($headers["ResetKey"],$id))
					write("Invalid Reset Key",true,200);
			}
			else
			{
				if(send_reset_email($id))
					write("E-Mail Sent",false,200);
				else
					write("Error",true,200);
			}

		}
			//Verify
			//Unset
			//Continue

		//If not, then check if header is set
			//If the header is set, verify
				//Verify
				//Unset
				//Continue
			//If not
				//Send email

		//If no header
		



			$password = $patch_data['password'];
			$hashed = secure_password($password);
			$patch_data["password"] = $hashed[0];
			$patch_data["salt"] = $hashed[1];
		
		//If header
			//
		
	}

	foreach($patch_data as $field => $patch)
	{
			$rows = $db->update("users",[
			$field => $patch,
			"#updated_on" => "NOW()"
			],[
			"id" => $id]);
			
	}

		

		$data = $db->select("users",[
			"id","first_name","last_name","email","city","state","country","zip_code","photo","birth_date","gender","own_privacy","hive_privacy"], ["id" => $id]);

		global $server_path;
		foreach($data as $item)
		{
			$item["photo"] = $server_path.$item["photo"];
		}
		

		$data[0]["links"] = getLinks($id,"user");
		$output['user'] = $data[0];	
		
		write($output,false,200);	
	


});

$app->delete("/user(/:id)","authenticate", function($id) use($app){
	global $user_id;
	
	is_owner($id);

	
	global $db;

	if($app->request->delete("test_mode") == true)
	{
		$db->delete("users", [
		"id" => $id	]);
	}
	else
	{
		$db->update("users",[
		"is_active"=> 0], ["id" => $id]);
	}

	/*
	$db->delete("users", [
		"id" => $id	]);*/

	write("Success",false,200);
});

$app->get("/users/search","authenticate",function() use($app)
{
	global $user_id;
	global $db;

	if(isset($_GET["email"]))
	{
		$email = $app->request->get("email");

		$user = $db->get("users",[
	"id", "first_name", "last_name", "email", "photo", "city", "state", "country", "zip_code", "gender", "hive_privacy", "own_privacy", "created_on"],
	["email" => $email]);

		if(!empty($user))
		{
			

			global $server_path;
	
			$user["photo"] = $server_path.$user["photo"];
			$user['links'] = getLinks($user['id'],"user");

			write($user,false,200);
		}
		else
			write("No Users found",true,200);
		
	}
	else
		write("Pass Proper Parameters",true,200);


});
