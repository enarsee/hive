<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/user/:id/promotions", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	
	$promotions = $db->select("promotions",["id","brand_id","is_read","promotion","added_on"],["user_id" => $id]);

	$output = array();

	foreach($promotions as $promotion)
	{
		if(array_key_exists($promotion["brand_id"], $output))
		{
			array_push($output[$promotion["brand_id"]]["promotions"], $promotion);
			$output[$promotion["brand_id"]]["meta"]["total"] += 1;
		}
		else
		{
			$output[$promotion["brand_id"]]["promotions"] = array();
			$output[$promotion["brand_id"]]["info"] = get_brand_info($promotion["brand_id"]);
			$output[$promotion["brand_id"]]["meta"]["total"] = 1;
			array_push($output[$promotion["brand_id"]]["promotions"], $promotion);
		}
	}

$final["brands"] = array();
    foreach($output as $item)
    array_push($final["brands"],$item);

	$final["meta"]["total"] = count($promotions);
	
	write($final,false,200);

});

$app->get("/user/:id/promotions/read", "authenticate",function($id) use($app){

	global $db;
	is_owner($id);
	
	$promotions = $db->select("promotions",["id","brand_id","is_read","promotion","added_on"],["AND" => ["user_id" => $id, "is_read" =>1]]);

	$output = array();

	foreach($promotions as $promotion)
	{
		if(array_key_exists($promotion["brand_id"], $output))
		{
			array_push($output[$promotion["brand_id"]]["promotions"], $promotion);
			$output[$promotion["brand_id"]]["meta"]["total"] += 1;
		}
		else
		{
			$output[$promotion["brand_id"]]["promotions"] = array();
			$output[$promotion["brand_id"]]["info"] = get_brand_info($promotion["brand_id"]);
			$output[$promotion["brand_id"]]["meta"]["total"] = 1;
			array_push($output[$promotion["brand_id"]]["promotions"], $promotion);
		}
	}
	$final["meta"]["total"] = count($promotions);
	$final["brands"] = $output;
	
	write($final,false,200);

});

$app->get("/user/:id/promotions/unread", "authenticate",function($id) use($app){

		global $db;
	is_owner($id);
	
	$promotions = $db->select("promotions",["id","brand_id","is_read","promotion","added_on"],["AND" => ["user_id" => $id, "is_read" =>0]]);

	$output = array();

	foreach($promotions as $promotion)
	{
		if(array_key_exists($promotion["brand_id"], $output))
		{
			array_push($output[$promotion["brand_id"]]["promotions"], $promotion);
			$output[$promotion["brand_id"]]["meta"]["total"] += 1;
		}
		else
		{
			$output[$promotion["brand_id"]]["promotions"] = array();
			$output[$promotion["brand_id"]]["info"] = get_brand_info($promotion["brand_id"]);
			$output[$promotion["brand_id"]]["meta"]["total"] = 1;
			array_push($output[$promotion["brand_id"]]["promotions"], $promotion);
		}
	}
	$final["meta"]["total"] = count($promotions);
	$final["brands"] = $output;
	
	write($final,false,200);

});

$app->get("/user/:id/promotion/:pid","authenticate",function($id,$pid) use($app){
	global $db;
	is_owner($id);

	$promotion = $db->get("promotions",["id","brand_id","is_read","promotion","added_on"],["AND" => ["id" => $pid,
		"user_id" => $id]]);

	if(!empty($promotion))
	{
		$promotion["brand"] = get_brand_info($promotion["brand_id"]);
	unset($promotion["brand_id"]);
		write($promotion,false,200);
	}
	else
		write("Not found",true,200);

});

$app->patch("/user/:id/promotion/:pid","authenticate",function($id,$pid) use($app){
	global $db;
	is_owner($id);

	$promotion = $db->get("promotions",["id","brand_id","is_read","promotion","added_on"],["AND" => ["id" => $pid,
		"user_id" => $id]]);



	if(!empty($promotion))
	{
		$db->update("promotions",["is_read" => 1],["id" => $pid]);
		if(!is_db_error())
		{
			$promotion = $db->get("promotions",["id","brand_id","is_read","promotion","added_on"],["AND" => ["id" => $pid,
				"user_id" => $id]]);
			$promotion["brand"] = get_brand_info($promotion["brand_id"]);
		unset($promotion["brand_id"]);
			write($promotion,false,200);
		}	
	}
	else
		write("Not found",true,200);

});

$app->delete("/user/:id/promotion/:pid","authenticate",function($id,$pid) use($app){
	global $db;
	is_owner($id);

	$promotion = $db->get("promotions",["id","brand_id","is_read","promotion","added_on"],["AND" => ["id" => $pid,
		"user_id" => $id]]);



	if(!empty($promotion))
	{
		$db->delete("promotions",["is_read" => 1],["id" => $pid]);
		if(!is_db_error())
		{
			
			write("Deleted Successfully",false,200);
		}	
	}
	else
		write("Not found",true,200);

});
