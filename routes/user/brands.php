<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING

$app->get("/user/:id/brands", "authenticate",function($id) use($app){

	global $db;

	$data = $db->select("honey",[
		"brand_id",
		"honey",
		"created_on",
		"updated_on"
		],[
		"user_id" => $id]);

	$locked = $db->select("lock",[
		"brand_id", "honey", "locked_for", "link_id", "locked_on"],["AND" => ["user_id" => $id,
		"is_locked" =>1 ]]);

	$output = array();

	$output["brands"]["available"] = array();
	$output["brands"]["locked"] = array();

	$output["meta"] = array();
	$total = 0;
	$total_l = 0;
	$count = count($data);

	foreach($data as $item){

		$total = $total + (float)$item['honey'];
		$links["brand"] = "/brand/".$item['brand_id'];
		$temp = get_brand_data_other_user($item["brand_id"],$id);
		$temp["links"] = $links;
		$item["links"] = $links;

		array_push($output["brands"]["available"],$temp);
	}

	$l_array = array();

	$total_l = 0;

	foreach($locked as $item){
		if(array_key_exists($item["brand_id"],$l_array))
		{
			
			array_push($l_array[$item["brand_id"]]["locked_honey"]["transactions"], $item);
			$l_array[$item["brand_id"]]["locked_honey"]["meta"]["total"] += (float)$item["honey"];
			$total_l += (float)$item["honey"];
		}
		else
		{
			$l_array[$item["brand_id"]] = array();
			$l_array[$item["brand_id"]]["info"] = get_brand_info($item["brand_id"]);
			$l_array[$item["brand_id"]]["locked_honey"] = array();
			$l_array[$item["brand_id"]]["locked_honey"]["transactions"] = array();
			array_push($l_array[$item["brand_id"]]["locked_honey"]["transactions"], $item);

			$l_array[$item["brand_id"]]["locked_honey"]["meta"]["total"] = (float)$item["honey"];
			$total_l += (float)$item["honey"];
		}

		
		
	}

	$temp = array();
	$output["brands"]["locked"]["brands"] = array();
	foreach($l_array as $key=>$item)
		array_push($temp,$item);

	$output["brands"]["locked"]["brands"]=$temp;

	$output["meta"]["total_brands"] = $count;
	$output["meta"]["total_honey"] = $total;
	$output["meta"]["locked_honey"] = $total_l;


	write($output,false,200);

});

$app->get("/user/:id/brand/:brand", "authenticate",function($id,$brand) use($app){

	global $db;

	
	

	$output = array();

	$output["brand"] = array();

	$output["brand"]["info"] = get_brand_info($brand);

	$output["available"] = array();

	$data = $db->get("honey",[
		"brand_id",
		"honey",
		"created_on",
		"updated_on"
		],[ "AND" => [
		"user_id" => $id,
		"brand_id" => $brand]]);

	if(!empty($data))
	{
		$output["available"] = get_user_brand_data($brand);
	}
	else
		$output["available"] = 0;

	$output["locked"] = array();

	$locked = $db->select("lock","*",[
		"AND" =>[
		"user_id" => $id,
		"brand_id" => $brand,
		"is_locked" => 1]]);

	if(!empty($locked))
	{


	$total = 0;
	$temp = array();

	foreach($locked as $item)
	{
		$total += (float)$item["honey"];
		array_push($temp,$item);
	}

	$output["locked"]["transactions"] = $temp;
	$output["locked"]["meta"]["total"] = $total;

	}
	else
		$output["locked"] = 0;




	/*
	
	

	$locked_honey = has_locked_honey($id,$brand);
		

	if(count($data)>0){
		$data = $data[0];
		$data["level_info"] = getLevel($data['honey'],$data['brand_id']);
		
		$data["links"] = getLinks($data['brand_id'],"brand");
		
		$data["locked"] = $locked_honey;
		$output["brand"] = $data;

	}
	else
	{
		if($locked_honey == false)
		{
			write("Entry does not exist",true,200);
		}
		else
		{
			$output["brand"] = $locked_honey;
		}
	}
	
	*/


	write($output,false,200);

});