<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING


//Get List of all hives

//Create a Hive
$app->get("/user/:id/purchases","authenticate",function($id) use($app){
	global $user_id;
	global $db;
	is_owner($id);

	$purchase_data = $db->select("purchases",["id","brand_id",
		"item_amount",
		"honey_applied",
		"honey_gained",
		"purchased_on"],["user_id" => $id]);

	foreach($purchase_data as $key=>$p)
	{
		$brand_data = getBrandData((int)$p["brand_id"]);
		unset($purchase_data[$key]["brand_id"]);
		$purchase_data[$key]["brand"]  = $brand_data;
	}

	$output = array();

	$output["purchases"] = $purchase_data;
	$output["meta"]["total"] = count($purchase_data);

	write($output,false,200);

});

