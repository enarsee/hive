<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//REGISTRATION END POINT
//Last Edited : 3rd December 2014 - Nishant
/*

URL : base/register/
Allowed : POST
Parameters Requried: name, email, password

Returned Output:

{
	"user": {
		"id": "36",
		"name": "Nishant Test",
		"email": "enarsee@gmail.com",
		"links": {
			"self": "\/user\/36",
			"brands": "\/user\/36\/brands",
			"favourites": "\/user\/36\/favourites"
		}
	},
	"error": false
}


*/

//ONLY EDIT IF YOU KNOW WHAT YOU ARE DOING


$app->get("/user/:id/hives","authenticate", function($id) use($app){
	global $user_id;
	
	is_owner($id);

	global $db;

	$hives = $db->select("hives", ["id","hive_id","joined_date","status"],[
		"user_id" => $id	]);

	$output = array();

	$output["hives"] = $hives;

	write($output,false,200);
});
