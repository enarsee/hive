<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
require 'vendor/autoload.php';

$db = new medoo();

$app = new \Slim\Slim();

$app_id = '101127';
$app_key = '0d68fe36e5344f139790';
$app_secret = '8fa7cf75f6b2ed095c6e';

global $pusher;

$pusher = new Pusher($app_key, $app_secret, $app_id);

$server_path = "http://api.hivetogether.com/";

function write($data,$has_error=false,$response)
{
	 $app = \Slim\Slim::getInstance();
    // Http response code

    // setting response content type to json
    $app->contentType('application/json');

    $app->status($response);


    if(is_array($data))
    	$data["error"] = $has_error;
    else
    {
    	$output = array();
    	$output['error']  = $has_error;
    	$output['message'] = $data;
    	$data = $output;
    }
 
    echo json_encode($data);
    
   $app->stop();
}


function valid_parameters($input,$type)
{
	$check = "";

	 $app = \Slim\Slim::getInstance();

	switch($type)
	{
		case "GET":$check = $_GET;break;
		case "POST":$check = $_POST;break;
		case "PUT":$check = $app->request->put();break;
		case "PATCH":$check = $_PATCH;break;
		case "DELETE":write("Wrong Request Type",400);break;

		default:write("Error",400);break;
	}

	$input = explode(",",$input);

	foreach($input as $key=>$item)
	{
		if(!isset($check[$item]))
		{
			$error = "Please pass proper parameters: ".implode(",",$input);
			write($error,true,400);
		}
		if(($item=="user_id" || $item=="brand_id" || strpos($item,"honey") || $item=="") &&  !is_numeric($check[$item]))
		{
			$error = "Please pass parameters in proper format";
			write($error,true,400);
			
		}
		unset($input[$key]);
	}
	return true;
}


function secure_password($password)
{
	$salt = substr(sha1(rand()), 0, 5);
	$hash = password_hash($password.$salt, PASSWORD_BCRYPT);

	return array($hash,$salt);
}




function check_login($email,$password,$flag=false)
{
	global $db;

	$data = $db->get("users", [
		"id",
	"password",
	"salt"], ["email" => $email]);



	if(!empty($data))

	{
	$salt = $data['salt'];
	$hash = $data['password'];
	$password = $password.$salt;
	$id = $data['id'];

	 if (password_verify($password, $hash)) {
	 	if($flag)
	 		return true;


	 	$api_key = substr(sha1(rand()), 0, 15);
	 	$expiry_time = date("Y-m-d H:i:s", strtotime('+5 hours'));

	 	$db->insert("authorization", [
	 "user_id" =>$id,
	 "api_key" => $api_key,
	 "expiry_time" => $expiry_time
	 ]);

	 	if($db->error()[2]!="")
	 	{
	 		$db->update("authorization", [
	 "api_key" => $api_key,
	 "expiry_time" => $expiry_time
	 ],["user_id" =>$id]);

	 	}

        return array($id,$api_key,$expiry_time);
    } else {
        return false;
    }
}
else
write("Invalid User",true,200);

}


function verify_key($api_key)
{
	global $db;
	$data = $db->select("authorization", [
		"user_id"],[
		"AND" =>
		["api_key" => $api_key, 
		"#expiry_time[>]" => "NOW()"
		]]);


	if(count($data)>0){
		$data = $data[0];

		$user_id = $data['user_id'];

		$user = $db->get("users",["is_active"],["id" => $user_id]);
		$user_active = $user["is_active"];
		if($user_active != 1)
			write("Account Deactivated",true,200);

		return array($user_id);

	}
	else return false;
}



function authenticate(\Slim\Route $route) {

	global $db;
    // Getting request headers
        $response = array();
    $app = \Slim\Slim::getInstance();
    $headers = $app->request->headers;


    if(isset($headers['ApiKey']))

  {

    $api_key = $headers['ApiKey'];

    $data = verify_key($api_key);

    if(is_array($data))
    {
    	global $user_id;
    	$user_id = $data[0];
    }
    else
    {
    	write("Access Denied",true,401);
    	$app->stop();
    }
}
else
{
	write("No API Key Specified",true,400);
	$app->stop();
}

}

function verify_plugin_key($key)
{
	global $db;

	$plugin = $db->get("plugins",["id","access_key"],["access_key"=> $key]);

	if(!empty($plugin))
		{
			if($key == $plugin["access_key"])
				return $plugin["id"];
			else
				write("Access Key Mismatch",true,200);
		}
	else
		write("Invalid Plugin Key",true,200);

}

function verify_request($id)
{

	global $db;
	$plugin = $db->get("plugins",["whitelist"],["id" => $id]);

	if(!empty($plugin))
	{
		 $app = \Slim\Slim::getInstance();
		 $referer = ($app->request->headers["Origin"]);

		$sites = explode(",", $plugin["whitelist"]);
		

		if((array_search($referer, $sites) >= 0) || $sites[0]=="*")
			return true;
		else
			write("Invalid Access",true,200);
	}
	else
		write("Invalid Plugin Key",true,200);
}

function plugin_authenticate(\Slim\Route $route) {

	global $db;
	global $plugin_id;
    // Getting request headers
    $app = \Slim\Slim::getInstance();
    $headers = $app->request->headers;

    //Get Plugin Key
    $plugin_key = $headers["PluginKey"];

    if(empty($plugin_key))
    	write("Unauthorized",true,200);

    //Verify If Correct
    $plugin_id = (verify_plugin_key($plugin_key));

    if(verify_request($plugin_id))
    	return true;
}

function send_reset_email($id)
{
	//Get Details
	global $db;
	$user = $db->get("users",["email"],["id" => $id]);
	$email = $user["email"];

	//Make reset key
	$reset_key = substr(sha1(rand()), 0, 6);



	//Email Reset Key
	// the message
	$to = "somebody@example.com";
$subject = "Hive : Password Reset";
$txt = "Heres your reset key ".$reset_key;
$headers = "From: webmaster@hive.com";

mail("enarsee@gmail.com",$subject,$txt,$headers);

	//Enter into database
	$reset = $db->get("password_reset",["user_id"],["user_id" => $id]);
	$expiry_time = date("Y-m-d H:i:s", strtotime('+2 hours'));

	if(!empty($reset))
	{

		$db->update("password_reset",["password_key" => $reset_key,
			"valid_till" => $expiry_time],["user_id" => $id]);


	}
	else
	{
		$db->insert("password_reset",["password_key" => $reset_key,
			"valid_till" => $expiry_time,
			"user_id" => $id]);
	}
	return true;

	//Return True
}

function check_reset_key($key,$id)
{
	global $db;



	$reset = $db->get("password_reset",["password_key"],["AND" => [
		"user_id" => $id,
		"#valid_till[>]" => "NOW()"]]);


	if(!empty($reset))
	{	

		$reset_key = $reset["password_key"];

		if($reset_key == $key)
			return true;
		else
			return false;
	}
	else
		return false;
}

function is_owner($id)
{
	global $user_id;

	if($id==-1)
		$id = $user_id;

	if(($id != $user_id))
	{
		write("Not Allowed",true,200);
	}
}

function is_message_owner($mid)
{
	global $user_id;
	global $db;

	$message = $db->get("messages",["sender_id","receiver_id"],[
		"id"=> $mid]);

	if(!empty($message))
	{
		if(($user_id == $message['sender_id']) || ($user_id == $message['receiver_id']))
		{
			return true;

		}
		else
		{
			write("Unauthorized",true,200);
		}
	}
	else
	{
		write("No such message", true, 200);
	}

}

function is_hive_owner($id,$hid)
{
	global $user_id;
	global $db;

	$hive = $db->select("hives",["id"],["AND" => ["owner_id"=>$id,
		"id"=>$hid]]);

	if(count($hive)>0)
		return true;
	else
		return false;

}

function is_hive_member($id,$hid,$status)
{
	global $user_id;
	global $db;

	$hive = $db->select("hive_members",["hive_id"], ["AND" => [
		"user_id" => $id,
		"hive_id" => $hid,
		"status" => $status]]);

	if(count($hive)>0)
		return true;
	else
		return false;
}

function is_notif_owner($nid)
{
	global $user_id;
	global $db;

	$notif = $db->select("notifications",["id"],["AND" => ["id" => $nid,
		"user_id" => $user_id]]);

	if(count($notif)>0)
		return true;
	else
		return false;


}


function getLinks($id,$type)
{
	$links = array();
	if($type=="user")
	{
		$links["self"] = "/user/".$id;
		$links["brands"] = "/user/".$id."/brands";
		$links["favourites"] = "/user/".$id."/favourites";

	}
	else if($type=="brand")
	{
		$links["self"] = "/brand/".$id;
	}
	return $links;
}

function getBrandData($id)
{
	global $db;

	$brand = $db->get("brands",[
	"id", "name", "description", "photo", "created_on"],["id"=>$id]);

	return $brand;


}

function in_array_multi($array,$index,$item)
{
	foreach($array as $i)
	{
		if($item == $i[$index])
			{return true;}
	}
	return false;
}
/*
function getBrandLevel($brand)
{
	global $db;


	//Get Level Data
	$level_data = $db->get("brand_levels",["max_level","increment"],["brand_id"=>$brand]);
	$max_level = (float)$level_data["max_level"];
	$increment = (float)$level_data["increment"];

	$output["max_level"] = $max_level;
	$output["increment"] = $increment;
	return $output;
}

function getLevel($honey,$brand)
{
	global $db;

	$honey = (float)$honey;

	//Get Level Data
	$level_data = $db->get("brand_levels",["max_level","increment"],["brand_id"=>$brand]);
	$max_level = (float)$level_data["max_level"];
	$increment = (float)$level_data["increment"];

	$level = 0;

	while(($honey >= ($level*$increment)))
	{
		$level = $level + 1;
	}

	if($level>$max_level)
	{
		$level = $max_level;
		$to_next = 0;
	}
	else
	{
		$to_next = (($level)*$increment) - $honey;
	}

	$output = array();
	$output["level"] = $level;
	$output["to_next"] = $to_next;

	return $output;

}
*/
function setNotification($sender_id,$n_type,$link_id,$n_content,$override=-1)
{
	global $db;
	global $user_id;

	$sender = $user_id;
	if($override!=-1)
		$sender = $override;
	$db->insert("notifications",[
		"user_id" => (int)$sender_id,
		"sender_id" => (int)$sender,
		"type_id" => $n_type,
		"link_id" => (int)$link_id,
		"message" => $n_content,
		"is_seen" => 0,
		"#created_on" => "NOW()"]);

	global $pusher;
	$data["message"] = $n_content;
	$data["type_id"] = $n_type;
	$data["link_id"] = $link_id;

	$channel_name = "hive_".(string)$sender_id;

	$pusher->trigger($channel_name, (string)$n_type, $data);

	
}


function getNotifications($n_type)
{
	global $db;
	global $user_id;

	$notifications = $db->select("notifications",
		["id","user_id","sender_id","type_id","message","is_seen","created_on"],
		["AND" => ["user_id" => $user_id,
		"type_id" => $n_type]]);

	return $notifications;

}


function debugDB()
{
	global $db;

	print_r($db->error());
	exit();
}

function get_user_data($id)
{
	global $db;
	$user = $db->get("users",["id","first_name","last_name","photo"],["id" =>$id]);

	return $user;
}

function get_user_brand_data($id,$flag=true)
{
	global $db;
	global $user_id;

	$honey = $db->get("honey",["honey"],["AND" => ["user_id" => $user_id,
		"brand_id" => $id]]);
	$brand = $db->get("brands",["id","name","description","photo"],["id" => $id]);

	



	if(!empty($honey))
	{
		$info = $brand;
		$honey = (float)$honey["honey"];

		//Get Locked Honey in a brand
		$locked_honey = (float)get_locked_honey($user_id,$id);
		
		$level_info = getLevel($honey+$locked_honey,$id);
	}
	else
	{
		$honey = false;
		$level_info = false;
	}

	$output = array();

	if($flag)
		$output["info"] = $brand;
	$output["honey"] = $honey;
	$output["level_info"] = $level_info;

	return $output;
}

function get_brand_data_other_user($id,$user_id,$flag=true)
{
	global $db;



	$honey = $db->get("honey",["honey"],["AND" => ["user_id" => $user_id,
		"brand_id" => $id]]);
	$brand = $db->get("brands",["id","name","description","photo"],["id" => $id]);

	

		
	if(!empty($honey))
	{
		$info = $brand;
		$honey = (float)$honey["honey"];
		//Get Locked Honey in a brand
		$locked_honey = (float)get_locked_honey($user_id,$id);


		$level_info = getLevel($honey+$locked_honey,$id);
	}
	else
	{
		$honey = false;
		$level_info = false;
	}

	$output = array();

	if($flag)
		$output["info"] = $brand;
	$output["honey"] = $honey;
	$output["level_info"] = $level_info;

	return $output;
}

function get_brand_info($id)
{
	global $db;
	global $user_id;

	
	$brand = $db->get("brands",["id","name","description","photo"],["id" => $id]);

	return $brand;
}


function get_brand_data($id,$honey,$flag=true)
{
	global $db;
	global $user_id;

	
	$brand = $db->get("brands",["id","name","description","photo"],["id" => $id]);


	
		$info = $brand;
		$level_info = getLevel($honey,$id);
	
	$output = array();
	if($flag)
		$output["info"] = $brand;
	$output["total_honey"] = $honey;
	$output["level_info"] = $level_info;

	return $output;
}

