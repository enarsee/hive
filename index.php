<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

include('includes.php');
include('util/image.php');
include('util/honey.php');
include('util/db.php');
include('util/offer.php');
include('util/hiveway.php');
include('util/level.php');
include('util/ads.php');

require("routes/auth/register.php");
require("routes/auth/login.php");

require("routes/user/fav.php");
require("routes/user/messages.php");
require("routes/user/promotions.php");
require("routes/user/profile.php");
require("routes/user/brands.php");
require("routes/user/hive.php");
require("routes/user/notif.php");
require("routes/user/offer.php");
require("routes/user/purchase.php");

require("routes/brand/info.php");

require("routes/hiveway/listings.php");
require("routes/hiveway/ads.php");

require("routes/plugin/plugin.php");

require("routes/merchant/promotions.php");
require("routes/merchant/ads.php");

$app->run();

?>



