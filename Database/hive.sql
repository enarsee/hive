-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 01, 2015 at 11:01 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hive`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `content` varchar(255) COLLATE utf8_bin NOT NULL,
  `link` varchar(255) COLLATE utf8_bin NOT NULL,
  `image` varchar(255) COLLATE utf8_bin NOT NULL,
  `date_added` datetime NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `brand_id`, `content`, `link`, `image`, `date_added`) VALUES
(1, 6, 'Testing', 'testing', 'testing', '2015-02-19 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `authorization`
--

CREATE TABLE IF NOT EXISTS `authorization` (
  `user_id` int(11) NOT NULL,
  `api_key` varchar(500) NOT NULL,
  `expiry_time` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`api_key`),
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `authorization`
--

INSERT INTO `authorization` (`user_id`, `api_key`, `expiry_time`) VALUES
(54, '1dc2ddb1c1502ad', '2015-02-02 17:08:36'),
(75, '4d57715372a2ceb', '2015-02-19 11:40:48'),
(82, 'be219961c053da3', '2015-02-06 12:52:16'),
(83, '511ef0f0168c946', '2015-02-04 12:56:19'),
(86, '57abf421e7c7d2d', '2015-02-10 11:09:46'),
(87, '2144812dd495300', '2015-02-10 12:44:50'),
(89, 'dc2962e2bbac4d8', '2015-03-01 17:39:50'),
(90, 'f2a4fe1839bb655', '2015-02-05 17:28:25'),
(91, 'c9e8665b0189016', '2015-02-06 18:39:26');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`, `photo`, `created_on`, `updated_on`) VALUES
(2, 'Nike', 'Nike, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing and worldwide marketing and selling of footwear, apparel, equipment, accessories and services.', 'http://api.hivetogether.com/images/brands/1.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(3, 'Apple', 'Apple Inc. is an American multinational corporation headquartered in Cupertino, California, that designs, develops, and sells consumer electronics, computer software, online services, and personal computers', 'http://api.hivetogether.com/images/brands/2.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(4, 'Levis', 'Levi Strauss & Co. is a privately held American clothing company known worldwide for its Levi''s brand of denim jeans', 'http://api.hivetogether.com/images/brands/3.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(5, 'Adidas', 'Adidas AG is a German multinational corporation that designs and manufactures sports shoes, clothing and accessories. The company is based in Herzogenaurach, Bavaria, Germany', 'http://api.hivetogether.com/images/brands/4.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(6, 'Dell', 'Dell Inc. is an American privately owned multinational computer technology company based in Round Rock, Texas, United States, that develops, sells, repairs and supports computers and related products and services', 'http://api.hivetogether.com/images/brands/5.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(7, 'Louis Vuitton', 'Louis Vuitton Malletier, commonly referred to as Louis Vuitton, or shortened to LV, is a French fashion house founded in 1854 by Louis Vuitton.', 'http://api.hivetogether.com/images/brands/6.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(8, 'GAP', 'The Gap, Inc., commonly known as Gap Inc. or Gap, is an American multinational clothing and accessories retailer. It was founded in 1969 by Donald Fisher and Doris F. Fisher and is headquartered in San Francisco, California.', 'http://api.hivetogether.com/images/brands/7.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(9, 'Tommy Hilfiger', 'Tommy Hilfiger is an American fashion, apparel, design, fragrance retail company, offering consumers high end products including men’s, women’s and children’s apparel, sportswear, denim,', 'http://api.hivetogether.com/images/brands/8.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(10, 'Reebok', 'Reebok International Limited was founded in 1895 in Bolton, Lancashire, England. It has been a subsidiary of the German group Adidas since 2005, and is a producer of athletic shoes, clothing, and accessories.', 'http://api.hivetogether.com/images/brands/10.png', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(11, 'Puma', 'Puma SE is a major German multinational company that produces athletic and casual footwear, as well as sportswear, headquartered in Herzogenaurach, Franconia, Germany.', 'http://api.hivetogether.com/images/brands/11.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(12, 'Rolex', 'Rolex SA and its subsidiary Montres Tudor SA design, manufacture, distribute and service wristwatches sold under the Rolex and Tudor brands', 'http://api.hivetogether.com/images/brands/12.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(13, 'Beats', 'Beats Electronics is a division of Apple Inc. which produces audio products. Headquartered in Culver City, California, U.S. the company was co-founded by rapper and hip hop producer Dr. Dre and Interscope-Geffen-A&M Records chairman Jimmy Iovine.', 'http://api.hivetogether.com/images/brands/13.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(14, 'Armani', 'Giorgio Armani S.p.A. is an Italian fashion house founded by Giorgio Armani which designs, manufactures, distributes and retails haute couture, ready-to-wear, leather goods, shoes, watches, jewelry', 'http://api.hivetogether.com/images/brands/14.png', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(15, 'Microsoft', 'Microsoft Corporation or is an American multinational corporation headquartered in Redmond, Washington, that develops, manufactures, licenses, supports and sells computer software, consumer electronics and personal computers and services. ', 'http://api.hivetogether.com/images/brands/15.png', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(16, 'Bose', 'The Bose Corporation is an American privately held corporation, based in Framingham, Massachusetts, that specializes in audio equipment.', 'http://api.hivetogether.com/images/brands/16.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00'),
(17, 'Sony', 'Sony Corporation, commonly referred to as Sony, is a Japanese multinational conglomerate corporation headquartered in K?nan Minato, Tokyo, Japan.', 'http://api.hivetogether.com/images/brands/17.jpg', '2014-11-14 00:00:00', '2014-11-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `brand_levels`
--

CREATE TABLE IF NOT EXISTS `brand_levels` (
  `brand_id` int(11) NOT NULL,
  `max_level` int(11) NOT NULL,
  `increment` int(11) NOT NULL,
  UNIQUE KEY `brand_id_3` (`brand_id`),
  KEY `brand_id` (`brand_id`),
  KEY `max_level` (`max_level`),
  KEY `increment` (`increment`),
  KEY `brand_id_2` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand_levels`
--

INSERT INTO `brand_levels` (`brand_id`, `max_level`, `increment`) VALUES
(2, 10, 10),
(3, 10, 10),
(4, 10, 10),
(5, 10, 10),
(6, 10, 10),
(7, 10, 10),
(8, 10, 10),
(9, 10, 10),
(10, 10, 10),
(11, 10, 10),
(12, 10, 10),
(13, 10, 10),
(14, 10, 10),
(15, 10, 10),
(16, 10, 10),
(17, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE IF NOT EXISTS `favourites` (
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`brand_id`),
  KEY `user_id` (`user_id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`user_id`, `brand_id`) VALUES
(54, 2),
(54, 3),
(54, 10),
(54, 12),
(54, 17),
(75, 2),
(75, 3),
(75, 4),
(75, 6),
(75, 8),
(91, 2),
(91, 3),
(91, 4),
(91, 5),
(91, 10),
(91, 11),
(91, 13),
(91, 14),
(91, 15),
(91, 16),
(91, 17);

-- --------------------------------------------------------

--
-- Table structure for table `hives`
--

CREATE TABLE IF NOT EXISTS `hives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_id_2` (`owner_id`),
  KEY `user_id` (`owner_id`),
  KEY `user_id_2` (`owner_id`),
  KEY `owner_id` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `hives`
--

INSERT INTO `hives` (`id`, `owner_id`, `created_on`) VALUES
(9, 75, '2014-12-08 14:05:12'),
(12, 54, '2014-12-16 11:53:12'),
(13, 86, '2015-02-05 12:07:59'),
(14, 90, '2015-02-05 12:09:31'),
(15, 87, '2015-02-05 13:26:48'),
(16, 91, '2015-02-06 13:38:38'),
(17, 89, '2015-02-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hive_members`
--

CREATE TABLE IF NOT EXISTS `hive_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hive_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hive_id` (`hive_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `hive_members`
--

INSERT INTO `hive_members` (`id`, `hive_id`, `user_id`, `status`, `created_date`) VALUES
(11, 9, 75, 0, '2014-12-08 14:05:12'),
(20, 12, 54, 0, '2014-12-16 11:53:12'),
(23, 9, 54, 1, '2015-01-02 12:18:12'),
(29, 12, 75, 1, '2015-01-16 08:03:54'),
(30, 13, 86, 0, '2015-02-05 12:07:59'),
(31, 14, 90, 0, '2015-02-05 12:09:31'),
(34, 15, 87, 0, '2015-02-05 13:26:48'),
(35, 17, 75, 1, '2015-02-18 00:00:00'),
(36, 17, 89, 0, '2015-02-18 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `honey`
--

CREATE TABLE IF NOT EXISTS `honey` (
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `honey` float NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`user_id`,`brand_id`),
  KEY `user_id` (`user_id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `honey`
--

INSERT INTO `honey` (`user_id`, `brand_id`, `honey`, `created_on`, `updated_on`) VALUES
(35, 6, 10, '2014-12-22 16:55:37', '2014-12-22 16:55:37'),
(54, 3, 4, '2015-01-06 12:52:56', '2015-02-19 06:41:50'),
(75, 2, 5, '2015-01-13 13:59:14', '2015-01-13 13:59:14'),
(75, 11, 13, '2015-01-09 12:14:03', '2015-01-09 12:14:03'),
(75, 14, 560, '2015-01-12 08:00:07', '2015-01-12 08:00:07'),
(82, 8, 13, '2015-01-14 21:48:25', '2015-01-14 21:48:25'),
(89, 2, 14, '2015-02-04 16:44:44', '2015-02-18 20:21:52');

-- --------------------------------------------------------

--
-- Table structure for table `level_info`
--

CREATE TABLE IF NOT EXISTS `level_info` (
  `brand_id` int(11) NOT NULL,
  `credit_threshold` varchar(500) NOT NULL,
  `earning_percentage` varchar(500) NOT NULL,
  `updated_on` datetime NOT NULL,
  UNIQUE KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level_info`
--

INSERT INTO `level_info` (`brand_id`, `credit_threshold`, `earning_percentage`, `updated_on`) VALUES
(2, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(3, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(4, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(5, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(6, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(7, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(8, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(9, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(10, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(11, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(12, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(13, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(14, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(15, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(16, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00'),
(17, '10,20,30,40,50,60,70,80,90,100', '1,3,5,7,9,11,13,15,17,19', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE IF NOT EXISTS `listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `offered_brand` int(11) NOT NULL,
  `offered_honey` float NOT NULL,
  `requested_brand` int(11) NOT NULL,
  `requested_honey` float NOT NULL,
  `is_hidden` int(11) NOT NULL,
  `listed_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `offered_brand` (`offered_brand`),
  KEY `requested_brand` (`requested_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`id`, `user_id`, `offered_brand`, `offered_honey`, `requested_brand`, `requested_honey`, `is_hidden`, `listed_on`) VALUES
(1, 75, 2, 0.5, 10, 0.5, 0, '2015-01-09 11:56:43'),
(2, 75, 12, 10, 7, 5, 0, '2015-01-09 12:07:12'),
(3, 75, 12, 5, 4, 5, 0, '2015-01-09 12:11:59'),
(4, 75, 11, 2.5, 9, 2, 0, '2015-01-09 12:14:36'),
(5, 75, 11, 2, 8, 5, 0, '2015-01-09 12:16:34'),
(6, 75, 11, 0.5, 16, 0.5, 0, '2015-01-09 12:18:40'),
(7, 75, 11, 3, 16, 3, 0, '2015-01-09 12:19:58'),
(8, 75, 11, 2, 6, 2, 0, '2015-01-09 12:21:39'),
(9, 75, 11, 1, 15, 1, 0, '2015-01-09 12:24:12'),
(10, 75, 11, 1, 12, 1, 0, '2015-01-09 12:25:31'),
(11, 82, 8, 2, 11, 5, 0, '2015-01-14 06:48:52'),
(12, 75, 14, 5, 9, 5, 0, '2015-01-15 09:31:20'),
(13, 75, 14, 10, 5, 20, 0, '2015-01-19 07:55:48');

-- --------------------------------------------------------

--
-- Table structure for table `lock`
--

CREATE TABLE IF NOT EXISTS `lock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `honey` float NOT NULL,
  `locked_on` datetime NOT NULL,
  `locked_for` int(11) NOT NULL,
  `is_locked` int(11) NOT NULL DEFAULT '1',
  `link_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `lock`
--

INSERT INTO `lock` (`id`, `user_id`, `brand_id`, `honey`, `locked_on`, `locked_for`, `is_locked`, `link_id`) VALUES
(6, 82, 8, 2, '2015-01-14 06:48:52', 1, 1, 11),
(7, 75, 14, 5, '2015-01-15 09:31:20', 1, 1, 12),
(8, 75, 14, 10, '2015-01-19 07:55:48', 1, 1, 13),
(9, 54, 3, 1, '2015-01-19 14:56:06', 0, 0, 7),
(10, 54, 3, 1, '2015-02-02 12:09:04', 0, 1, 8),
(11, 82, 8, 5, '2015-02-06 07:56:32', 0, 1, 9),
(12, 89, 2, 2, '2015-02-18 20:15:08', 0, 1, 10),
(13, 89, 2, 2, '2015-02-18 20:15:23', 0, 1, 11),
(14, 89, 2, 2, '2015-02-18 20:15:25', 0, 1, 12),
(15, 89, 2, 2, '2015-02-18 20:15:26', 0, 0, 13);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `is_read` int(11) NOT NULL,
  `sent_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`,`receiver_id`),
  KEY `receiver_id` (`receiver_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `receiver_id`, `message`, `is_read`, `sent_date`) VALUES
(6, 75, 75, 'Second message for pkim', 1, '2014-12-10 14:48:54'),
(8, 75, 83, 'Hi, this is Peter Kim1.', 1, '2014-12-17 06:55:15'),
(9, 54, 75, 'lkjhg', 1, '2014-12-17 09:57:34'),
(15, 54, 75, 'asdasd', 1, '2014-12-31 12:32:35'),
(16, 54, 75, 'asdasdf', 1, '2014-12-31 12:34:04'),
(17, 54, 75, 'Hey, its Tommy.', 1, '2014-12-31 12:44:06'),
(18, 54, 75, 'dur de dur de dur', 1, '2014-12-31 12:55:39'),
(19, 54, 75, 'dur de dur de dur', 1, '2014-12-31 12:55:41'),
(20, 54, 75, 'dur de dur de dur', 1, '2014-12-31 12:55:42'),
(55, 75, 0, '', 0, '2015-01-08 07:50:59'),
(56, 75, 83, 'THis is a message.', 1, '2015-01-08 08:09:52'),
(57, 54, 75, '54 to 75 - Message 1', 1, '2015-01-16 06:27:19'),
(58, 54, 75, '54 to 75 - Message 2', 1, '2015-01-16 06:30:31'),
(59, 54, 75, '54 to 75 - this is a longer message to see how the css handles longer messages in buzzbox.', 1, '2015-01-16 06:39:36'),
(60, 54, 75, '54 to 75 - this is a longer message to see how the css handles longer messages in buzzbox 2.', 1, '2015-01-16 07:29:38'),
(61, 54, 75, '54 to 75 - this is a longer message to see how the css handles longer messages in buzzbox 3.', 1, '2015-01-16 07:35:12'),
(62, 75, 54, 'Hey Tommy!', 1, '2015-01-16 09:42:32'),
(63, 75, 0, 'Hey Tommy!', 0, '2015-01-16 09:45:24'),
(64, 75, 54, 'hey, its Pete again.', 1, '2015-01-16 11:34:48'),
(65, 75, 54, 'hey, its pete once again. testing 1.', 1, '2015-01-16 11:46:32'),
(66, 75, 54, 'Testing 2', 1, '2015-01-16 11:47:35'),
(67, 75, 54, '3413rrgvfsvsf', 1, '2015-01-16 11:50:47'),
(68, 75, 54, 'po35it90dfgblkdfb', 1, '2015-01-16 11:55:13'),
(69, 75, 54, 'adsfsdf', 1, '2015-01-16 11:57:43'),
(70, 75, 54, 'this is a really long message to test whether of not the css of the message messes up the css of the conversation view', 1, '2015-01-16 12:00:17'),
(71, 54, 75, 'Hey, I updated the text box in buzzbox.', 1, '2015-01-19 09:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `type_id` varchar(255) NOT NULL,
  `link_id` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `is_seen` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`type_id`),
  KEY `type_id` (`type_id`),
  KEY `user_id_2` (`user_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=125 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `sender_id`, `type_id`, `link_id`, `message`, `is_seen`, `created_on`) VALUES
(11, 75, 75, '11', 6, 'You have a new message!', 0, '2014-12-10 14:48:54'),
(26, 75, 54, '11', 9, 'You have a new message!', 0, '2014-12-17 09:57:34'),
(34, 75, 54, 'message_received', 15, 'You have a new message!', 0, '2014-12-31 12:32:35'),
(35, 75, 54, 'message_received', 16, 'You have a new message!', 0, '2014-12-31 12:34:04'),
(36, 75, 54, 'message_received', 17, 'You have a new message!', 0, '2014-12-31 12:44:06'),
(37, 75, 54, 'message_received', 18, 'You have a new message!', 0, '2014-12-31 12:55:39'),
(38, 75, 54, 'message_received', 19, 'You have a new message!', 0, '2014-12-31 12:55:41'),
(39, 75, 54, 'message_received', 20, 'You have a new message!', 0, '2014-12-31 12:55:42'),
(40, 54, 75, 'hive_invited', 9, 'You have been invited to join a Hive !', 0, '2015-01-02 12:18:12'),
(54, 75, 54, 'hive_accepted', 9, 'Your Hive Invitation has been accepted', 0, '2015-01-05 08:19:50'),
(55, 75, 54, 'hive_invited', 12, 'You have been invited to join a Hive !', 0, '2015-01-05 12:06:00'),
(56, 54, 75, 'hive_accepted', 12, 'Your Hive Invitation has been accepted', 0, '2015-01-05 12:06:17'),
(85, 75, 54, 'offer_received', 3, 'You have a new offer !', 0, '2015-01-13 13:59:41'),
(86, 75, 54, 'offer_received', 4, 'You have a new offer !', 0, '2015-01-13 14:06:44'),
(87, 75, 54, 'offer_received', 5, 'You have a new offer !', 0, '2015-01-13 14:08:23'),
(88, 54, 75, 'offer_received', 6, 'You have a new offer !', 0, '2015-01-13 14:16:11'),
(89, 75, 54, 'message_received', 57, 'You have a new message!', 0, '2015-01-16 06:27:19'),
(90, 75, 54, 'message_received', 58, 'You have a new message!', 0, '2015-01-16 06:30:31'),
(91, 75, 54, 'message_received', 59, 'You have a new message!', 0, '2015-01-16 06:39:36'),
(92, 75, 54, 'message_received', 60, 'You have a new message!', 0, '2015-01-16 07:29:38'),
(93, 75, 54, 'message_received', 61, 'You have a new message!', 0, '2015-01-16 07:35:12'),
(94, 75, 54, 'hive_invited', 12, 'You have been invited to join a Hive !', 0, '2015-01-16 07:43:17'),
(95, 54, 75, 'hive_accepted', 12, 'Your Hive Invitation has been accepted', 0, '2015-01-16 07:58:13'),
(96, 75, 54, 'hive_invited', 12, 'You have been invited to join a Hive !', 0, '2015-01-16 07:59:11'),
(97, 54, 75, 'hive_rejected', 12, 'Your Hive Invitation has been rejected', 0, '2015-01-16 08:00:10'),
(98, 75, 54, 'hive_invited', 12, 'You have been invited to join a Hive !', 0, '2015-01-16 08:00:18'),
(99, 54, 75, 'hive_rejected', 12, 'Your Hive Invitation has been rejected', 0, '2015-01-16 08:02:41'),
(100, 75, 54, 'hive_invited', 12, 'You have been invited to join a Hive !', 0, '2015-01-16 08:02:46'),
(101, 54, 75, 'hive_rejected', 12, 'Your Hive Invitation has been rejected', 0, '2015-01-16 08:03:46'),
(102, 75, 54, 'hive_invited', 12, 'You have been invited to join a Hive !', 0, '2015-01-16 08:03:54'),
(103, 54, 75, 'hive_accepted', 12, 'Your Hive Invitation has been accepted', 0, '2015-01-16 08:04:02'),
(104, 54, 75, 'message_received', 62, 'You have a new message!', 0, '2015-01-16 09:42:32'),
(106, 54, 75, 'message_received', 64, 'You have a new message!', 0, '2015-01-16 11:34:48'),
(107, 54, 75, 'message_received', 65, 'You have a new message!', 0, '2015-01-16 11:46:32'),
(108, 54, 75, 'message_received', 66, 'You have a new message!', 0, '2015-01-16 11:47:35'),
(109, 54, 75, 'message_received', 67, 'You have a new message!', 0, '2015-01-16 11:50:47'),
(110, 54, 75, 'message_received', 68, 'You have a new message!', 0, '2015-01-16 11:55:13'),
(111, 54, 75, 'message_received', 69, 'You have a new message!', 0, '2015-01-16 11:57:43'),
(112, 54, 75, 'message_received', 70, 'You have a new message!', 0, '2015-01-16 12:00:17'),
(113, 75, 54, 'message_received', 71, 'You have a new message!', 0, '2015-01-19 09:06:43'),
(114, 75, 54, 'offer_received', 7, 'You have a new offer !', 0, '2015-01-19 14:56:06'),
(115, 75, 54, 'offer_received', 8, 'You have a new offer !', 0, '2015-02-02 12:09:04'),
(119, 75, 82, 'offer_received', 9, 'You have a new offer !', 0, '2015-02-06 07:56:32'),
(120, 75, 89, 'offer_received', 10, 'You have a new offer !', 0, '2015-02-18 20:15:08'),
(121, 75, 89, 'offer_received', 11, 'You have a new offer !', 0, '2015-02-18 20:15:23'),
(122, 75, 89, 'offer_received', 12, 'You have a new offer !', 0, '2015-02-18 20:15:25'),
(123, 75, 89, 'offer_received', 13, 'You have a new offer !', 0, '2015-02-18 20:15:26'),
(124, 54, 75, 'offer_rejected', 7, 'Your offer has been rejected !', 0, '2015-02-19 06:41:50');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `offered_honey` float NOT NULL,
  `requested_honey` float NOT NULL,
  `offered_brand` int(11) NOT NULL,
  `requested_brand` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `from_listing` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`,`receiver_id`,`offered_brand`,`requested_brand`),
  KEY `sender_id_2` (`sender_id`),
  KEY `receiver_id` (`receiver_id`),
  KEY `offered_brand` (`offered_brand`),
  KEY `requested_brand` (`requested_brand`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `sender_id`, `receiver_id`, `offered_honey`, `requested_honey`, `offered_brand`, `requested_brand`, `status`, `from_listing`, `created_on`, `updated_on`) VALUES
(3, 54, 75, 1, 0.5, 3, 2, 4, 0, '2015-01-13 13:59:41', '2015-01-13 13:59:41'),
(4, 54, 75, 1, 0.5, 3, 2, 4, 0, '2015-01-13 14:06:44', '2015-01-13 14:06:44'),
(5, 54, 75, 1, 0.5, 3, 2, 4, 0, '2015-01-13 14:08:23', '2015-01-13 14:08:23'),
(6, 75, 54, 0.5, 1, 2, 3, 0, 0, '2015-01-13 14:16:11', '2015-01-13 14:16:11'),
(7, 54, 89, 1, 10, 3, 14, 3, 0, '2015-01-19 14:56:06', '2015-01-19 14:56:06'),
(8, 54, 75, 1, 5, 3, 14, 0, 0, '2015-02-02 12:09:04', '2015-02-02 12:09:04'),
(9, 82, 75, 5, 5, 8, 14, 0, 0, '2015-02-06 07:56:32', '2015-02-06 07:56:32'),
(10, 89, 75, 2, 5, 2, 14, 0, 0, '2015-02-18 20:15:08', '2015-02-18 20:15:08'),
(11, 89, 75, 2, 5, 2, 14, 1, 0, '2015-02-18 20:15:23', '2015-02-18 20:15:23'),
(12, 89, 75, 2, 5, 2, 14, 2, 0, '2015-02-18 20:15:25', '2015-02-18 20:15:25'),
(13, 89, 75, 2, 5, 2, 14, 4, 0, '2015-02-18 20:15:26', '2015-02-18 20:15:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE IF NOT EXISTS `password_reset` (
  `user_id` int(11) NOT NULL,
  `password_key` varchar(255) NOT NULL,
  `valid_till` datetime NOT NULL,
  UNIQUE KEY `user_id_2` (`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `whitelist` varchar(512) NOT NULL,
  `access_key` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `brand_id`, `whitelist`, `access_key`, `is_active`, `created_on`) VALUES
(1, 'a', 2, '*', 'zVkU1', 1, '0000-00-00 00:00:00'),
(2, 'b', 3, '*', 'go997', 1, '0000-00-00 00:00:00'),
(3, 'c', 4, '*', 'HyER2', 1, '0000-00-00 00:00:00'),
(4, 'd', 5, '*', 'oZ48S', 1, '0000-00-00 00:00:00'),
(5, 'e', 6, '*', 'QI1Vy', 1, '0000-00-00 00:00:00'),
(6, 'f', 7, '*', 'Ot5ID', 1, '0000-00-00 00:00:00'),
(7, 'g', 8, '*', 'CbrZU', 1, '0000-00-00 00:00:00'),
(8, 'h', 9, '*', 'dAx72', 1, '0000-00-00 00:00:00'),
(9, 'i', 10, '*', 'TX7aR', 1, '0000-00-00 00:00:00'),
(10, 'j', 11, '*', 'QpU8m', 1, '0000-00-00 00:00:00'),
(11, 'k', 12, '*', 'R0YnK', 1, '0000-00-00 00:00:00'),
(12, 'l', 13, '*', 'boHwV', 1, '0000-00-00 00:00:00'),
(13, 'm', 14, '*', 'lzGe1', 1, '0000-00-00 00:00:00'),
(14, 'n', 15, '*', '5wdXr', 1, '0000-00-00 00:00:00'),
(15, 'o', 16, '*', 'ZOR7b', 1, '0000-00-00 00:00:00'),
(16, 'p', 17, '*', 'jbUwL', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `promotion` varchar(500) NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  `added_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`),
  KEY `brand_id` (`brand_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `brand_id`, `user_id`, `promotion`, `is_read`, `added_on`) VALUES
(7, 2, 35, 'Bored', 0, '2015-01-09 22:02:52'),
(9, 6, 54, 'Hey', 0, '2015-01-09 07:10:56'),
(10, 6, 75, 'Hey', 0, '2015-01-09 07:10:56'),
(11, 6, 82, 'Hey', 0, '2015-01-09 07:10:56');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE IF NOT EXISTS `purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `plugin_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_amount` float NOT NULL,
  `honey_applied` float NOT NULL,
  `honey_gained` float NOT NULL,
  `purchased_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`brand_id`,`plugin_id`),
  KEY `brand_id` (`brand_id`),
  KEY `plugin_id` (`plugin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `purchases`
--

INSERT INTO `purchases` (`id`, `user_id`, `brand_id`, `plugin_id`, `item_id`, `item_amount`, `honey_applied`, `honey_gained`, `purchased_on`) VALUES
(12, 75, 2, 1, 0, 50, 0, 0.5, '2014-12-22 15:46:00'),
(13, 35, 6, 5, 0, 1000, 0, 10, '2014-12-22 16:55:37'),
(15, 54, 3, 2, 0, 500, 0, 5, '2015-01-06 12:52:56'),
(16, 75, 12, 11, 0, 1500, 0, 15, '2015-01-09 09:10:10'),
(17, 75, 11, 10, 0, 2500, 0, 25, '2015-01-09 12:14:03'),
(18, 75, 14, 13, 0, 57500, 0, 575, '2015-01-12 08:00:07'),
(19, 75, 2, 1, 0, 550, 0, 5.5, '2015-01-13 13:59:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(500) NOT NULL,
  `salt` varchar(500) NOT NULL,
  `photo` longtext NOT NULL,
  `gender` varchar(1) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zip_code` int(20) NOT NULL,
  `birth_date` date NOT NULL,
  `hive_privacy` int(11) NOT NULL,
  `own_privacy` int(11) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL,
  `updated_on` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=92 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `salt`, `photo`, `gender`, `city`, `state`, `country`, `zip_code`, `birth_date`, `hive_privacy`, `own_privacy`, `is_active`, `created_on`, `updated_on`) VALUES
(35, '', '', 'ahelm3@gmail.com', '$2y$10$QQIVj2/VXfF4cvSbEjojNezTyI2ASvhaOwuId1wLzpJP4AmKP.KY.', '21616', '', '', '', '', '', 0, '0000-00-00', 0, 0, 1, '2014-11-14 13:06:11', '2014-11-14 13:06:11'),
(54, 'Tommy', 'Golightly', 'tgolightly@sparknettech.com', '$2y$10$Ad7XOBM1eTnvnjxslvA8KOgEl9PXCn0.7xuYa.l1oPjcozgoVV1Bi', '81640', 'images/users/54.png', 'M', 'Wayne', '', '', 19437, '1991-01-01', 0, 0, 1, '2014-12-04 12:34:51', '2015-01-19 07:41:51'),
(75, 'Peter', 'Kim1', 'pkim@sparknettech.com', '$2y$10$AwxWX0JcCmynJj7XMoS6EOKCsnAJF9Nx8/GCumB5p9/6SgmjyY.pe', 'd5b8a', 'images/users/75.png', 'M', 'Wayne', 'PA', 'US', 19437, '1990-10-30', 0, 1, 1, '2014-12-08 13:33:39', '2015-01-02 12:02:08'),
(77, 'John', 'Doe', 'john.doe@hive.com', '$2y$10$xcqHnTXydjLnmU4TOCM35uTYnScmNumrZt0CLrUf1EwZ6ea92kdSG', 'e33da', '', '', 'New York', 'New York', 'USA', 600111, '0000-00-00', 0, 0, 1, '2014-12-08 17:44:04', '2014-12-08 17:44:04'),
(79, 'John', 'Doe', 'jane.doe@hive.com', '$2y$10$44rrxoxYg5bhGyHxMAxGAu69m2ufMkVsJWGBVVrx850mJ5y0WOeRa', '5c17c', '', 'F', 'New York', 'New York', 'USA', 600111, '1993-10-31', 0, 0, 1, '2014-12-09 07:46:46', '2014-12-09 07:46:46'),
(82, 'Test', 'Value', 'enarsee@gmail.com', '$2y$10$PU1z5bM2ukahu2iArGAyjubu5uaMJW8K/G9oEVZsnSlmVf7h6mpXO', 'dccd3', 'images/users/82.png', 'M', 'Singapore', 'Singapore', 'Singapore', 400705, '1993-10-31', 0, 0, 1, '2014-12-15 12:04:29', '2014-12-15 12:04:29'),
(83, 'Peter', 'Kim', 'peyterkim@yahoo.com', '$2y$10$E6S5Wy.G.zxeBlEmxlm4ieh3QgwHx0juOkHmr3JjiMcdk4rku9cAm', '2ede3', '', 'M', 'Ambler', 'PA', 'US', 19437, '1990-10-20', 0, 0, 0, '2015-01-19 10:49:11', '2015-01-19 10:49:11'),
(85, 'Peter', 'Kim', 'peyterjoonsungkim@gmail.com', '$2y$10$zFiQ4kcqWW.nRJ5mdslweuLhux.XSnwsWvxz87KAF0beZjrC3tn.6', '2b25c', '', 'M', 'Ambler', 'PA', 'US', 19437, '1990-10-20', 0, 0, 1, '2015-01-19 10:49:43', '2015-01-19 10:49:43'),
(86, 'Jonathan', 'De Jong', 'jdejong@sparknettech.com', '$2y$10$MzT3hsRJF1DyFiDyJ4vXyOaQdSmCJk1jRZdYUhCRpinaBrNSXooX6', '8a1a5', 'images/users/86.jpg', 'M', 'Wayne', 'PA', 'US', 19437, '1983-06-07', 0, 0, 1, '2015-01-21 10:31:14', '2015-01-21 10:31:14'),
(87, 'Carl', 'Partridge', 'test@gmail.com', '$2y$10$UqZ4RT1dN3hDWqaJin5XLuV00t4aAIwYGNErDdzG7GQJrrUS4yhdy', '4c296', '', 'M', 'Bethlehem', 'PA', 'US', 19437, '1989-05-05', 0, 0, 1, '2015-02-02 08:58:53', '2015-02-02 08:58:53'),
(89, 'Test', 'User', 'nishant@enarsee.com', '$2y$10$AwxWX0JcCmynJj7XMoS6EOKCsnAJF9Nx8/GCumB5p9/6SgmjyY.pe', 'd5b8a', '', 'M', 'Singapore', 'Singapore', 'Singapore', 637658, '0000-00-00', 0, 0, 1, '2015-02-04 01:38:08', '2015-02-04 01:38:08'),
(90, 'Peter', 'Kim3', 'peterkim@test.com', '$2y$10$geF/7VHuWC7Xdj3.Up6jKOq4xq.QZddRN6RNr1yAQA64qlA/1t//K', '53647', 'images/users/90.png', 'M', 'Ambler', 'PA', 'US', 19437, '1990-10-30', 0, 0, 1, '2015-02-04 08:07:38', '2015-02-04 08:07:38'),
(91, 'Austin', 'Helm', 'a.s.helm3@gmail.com', '$2y$10$WN4syPvuHImfA9KHdVauzeqnE6UvNzyKHPyA5yaFtqqoCZR.b9pQG', '48ef6', 'images/users/91.JPG', 'M', 'Wyomissing', 'Pennsylvania', 'US', 19437, '0000-00-00', 0, 0, 1, '2015-02-06 13:38:38', '2015-02-06 13:38:38');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ads`
--
ALTER TABLE `ads`
  ADD CONSTRAINT `ads_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `authorization`
--
ALTER TABLE `authorization`
  ADD CONSTRAINT `authorization_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `brand_levels`
--
ALTER TABLE `brand_levels`
  ADD CONSTRAINT `brand_levels_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `favourites`
--
ALTER TABLE `favourites`
  ADD CONSTRAINT `favourites_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `favourites_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hives`
--
ALTER TABLE `hives`
  ADD CONSTRAINT `hives_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `hive_members`
--
ALTER TABLE `hive_members`
  ADD CONSTRAINT `hive_members_ibfk_1` FOREIGN KEY (`hive_id`) REFERENCES `hives` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `hive_members_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `honey`
--
ALTER TABLE `honey`
  ADD CONSTRAINT `honey_brands` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `honey_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `level_info`
--
ALTER TABLE `level_info`
  ADD CONSTRAINT `level_info_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `listings`
--
ALTER TABLE `listings`
  ADD CONSTRAINT `listings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `listings_ibfk_2` FOREIGN KEY (`offered_brand`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `listings_ibfk_3` FOREIGN KEY (`requested_brand`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lock`
--
ALTER TABLE `lock`
  ADD CONSTRAINT `lock_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `lock_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offers_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offers_ibfk_3` FOREIGN KEY (`offered_brand`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offers_ibfk_4` FOREIGN KEY (`requested_brand`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD CONSTRAINT `password_reset_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `plugins`
--
ALTER TABLE `plugins`
  ADD CONSTRAINT `plugins_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`);

--
-- Constraints for table `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `promotions_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `promotions_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `purchases_ibfk_3` FOREIGN KEY (`plugin_id`) REFERENCES `plugins` (`id`),
  ADD CONSTRAINT `purchases_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
