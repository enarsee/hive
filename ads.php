<title>Server Response</title>
<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

$_POST = array_filter($_POST);

$brand_id = $_POST["brand_id"];

unset($_POST["brand_id"]);


$service_url = 'http://localhost/ads';

$curl = curl_init($service_url);

$curl_post_data = $_POST;
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'PluginKey: '.$brand_id
    ));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);

$curl_response = curl_exec($curl);

if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
curl_close($curl);

echo (json_encode($curl_response, JSON_PRETTY_PRINT));

?>
<br>
<a href='ads-test.html'>Go Back</a>