<?php
//Model Class for Hiveway Transactions
function getBrandLevel($brand_id)
{
	global $db;

	$level_info = $db->get("level_info",["credit_threshold","earning_percentage"],["brand_id" => $brand_id]);

	if(!empty($level_info))
	{
		$credit_threshold = explode(",", $level_info["credit_threshold"]);
		$earning_percentage = explode(",", $level_info["earning_percentage"]);

		$output = array();
		foreach($credit_threshold as $key=>$c)
		{
			$temp = array();
			$temp["credit_threshold"] = $c;
			$temp["earning_percentage"] = $earning_percentage[$key];
			$output[(int)$key + 1] = $temp;
		}
		return $output;

	}
}

function getLevel($honey,$brand_id)
{
		global $db;

	$level_info = $db->get("level_info",["credit_threshold","earning_percentage"],["brand_id" => $brand_id]);
	if(!empty($level_info))
	{
			$honey = (float)$honey;
			//Get Locked Honey
			
			
			$level = 0;
			$to_next = 0;
			$ep = 0;
			$credit_threshold = explode(",", $level_info["credit_threshold"]);
			$earning_percentage = explode(",", $level_info["earning_percentage"]);

			foreach($credit_threshold as $key=>$c)
			{
				$c = (float)$c;
				if($honey<$c)
				{
					$level = (float)$key + 1;
					$to_next = $c - $honey;
					$ep = (float)$earning_percentage[$key];
					break;
				}


			}

			$output["level"] = $level;
			$output["to_next"] = $to_next;
			$output["earning_percentage"] = $ep;

			return $output;

	}
}


