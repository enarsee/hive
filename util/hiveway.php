<?php
//Model Class for Hiveway Transactions

function owns_listing($lid)
{
	global $db;
	global $user_id;

	$listing = $db->get("listings",["user_id"],["AND" => ["id" => $lid,
		"is_hidden" => 0]]);

	if(!empty($listing))
	{
		if($listing["user_id"] == $user_id)
			return true;
		else
			write("Unauthorized",true,200);
	}
	else
		write("Listing does not exist",true,200);
}

function owns_ad($aid)
{

}