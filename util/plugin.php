<?php
//Model Class for Hiveway Transactions
global $db;

function verify_plugin_key($key)
{
	$plugin = $db->get("plugins",["id"],["plugin_key"=> $key]);

	if(!empty($plugin))
		return $plugin["id"];
	else
		write("Invalid Plugin Key",true,200);

}

function verify_request($id)
{
	$plugin = $db->get("plugins",["whitelist"],["id" => $id]);

	if(!empty($plugin))
	{
		$referer = $_SERVER['HTTP_REFERER'];
		$sites = split(",", $plugin["whitelist"]);

		if(array_search($referer, $sites))
			return true;
		else
			write("Invalid Access",true,200);
	}
	else
		write("Invalid Plugin Key",true,200);
}