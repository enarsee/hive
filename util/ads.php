<?php
//Model Class for Hiveway Transactions
global $db;

function get_ads()
{
	global $db;

	$ads = $db->select("ads",["id",
		"brand_id",
		"content",
		"link",
		"image",
		"date_added"]);



	if(!empty($ads))
	{
	foreach($ads as $ad)
	{
		$ad["brand"] = get_brand_info((int)$ad["brand_id"]);
		unset($ad["brand_id"]);
	}

	$output["advertisements"] = $ads;
	$output["meta"]["total"] = count($ads);

	return $output;
}
else
	write("No ads",true,200);
}

function get_ad($id)
{
	global $db;

	$ad = $db->get("ads",["id",
		"brand_id",
		"content",
		"link",
		"image",
		"date_added"],
		["id" => $id]);

	if(!empty($ad))
	{
		$ad["ad_id"] = $ad["id"];
		unset($ad["id"]);
		$ad["brand"] = get_brand_info((int)$ad["brand_id"]);

		unset($ad["brand_id"]);
		$output= array();
		$output["advertisement"] = $ad;


		return $output;
	}
	else
		write("Not found",true,200);
}


