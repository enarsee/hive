<?php
//Model Class for Hiveway Transactions

function owns_offer($oid)
{
	global $db;
	global $user_id;

	$offer = $db->get("offers",["id"],["AND" =>
		["id" => $oid,
		"sender_id" => $user_id]]);
	if(!empty($offer))
		return true;
	else
	{
		return false;
		write("Unauthorized",true,200);
	}
}

function has_offer($oid)
{
	global $db;
	global $user_id;



	$offer = $db->get("offers",["id"],["AND" =>
		["id" => $oid,
		"OR" => ["sender_id" => $user_id,
		"receiver_id" => $user_id]]]);

	if(!empty($offer))
		return true;
	else
	{
		return false;
		write("Unauthorized",true,200);
	}
}

function has_valid_offer($oid)
{
	global $db;
	global $user_id;


	
	$offer = $db->get("offers",["id"],["AND" =>
		["id" => $oid,
		"status" => 0,
		"OR" => ["receiver_id" => (int)$user_id]]]);

	if(!empty($offer))
		return true;
	else
	{
		return false;
		write("Unauthorized",true,200);
	}
}



function make_offer($user_id,$offered_brand,$offered_honey,$receiver_id,$requested_brand,$requested_honey,$listing_id=0)
{
	global $db;

		if((has_honey($user_id,$offered_brand,$offered_honey)) && (has_honey($receiver_id,$requested_brand,$requested_honey)))
		{
			//lock_honey($user_id,$offered_brand,$offered_honey);

			$offer_id = $db->insert("offers",["sender_id"=>$user_id,
				"receiver_id" => $receiver_id,
				"offered_brand" => $offered_brand,
				"offered_honey" => $offered_honey,
				"requested_honey" => $requested_honey,
				"requested_brand" => $requested_brand,
				"from_listing" => $listing_id,
				"#created_on" => "NOW()",
				"#updated_on" => "NOW()"]);

			if(!is_db_error())
				{
					lock_honey($user_id,$offered_brand,$offered_honey,0,$offer_id);
					
					if($listing_id!=0)
						{
							setNotification($receiver_id,"offer_received",$offer_id,"You have an offer from a listing !");

						}
					else
						setNotification($receiver_id,"offer_received",$offer_id,"You have a new offer !");
					
					$offer = $db->get("offers","*",["id" => $offer_id]);
					$output["offer"] = $offer;

					write($output,false,201);
				}
			else
			{
				write("DB Error",true,200);
			}



		}
}

function cancel_offer($oid)
{

}

function accept_offer($oid)
{

}

function reject_offer($oid)
{

}

function counter_offer($oid,$sent_brand,$sent_honey,$received_brand,$received_honey)
{

}

