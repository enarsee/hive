<?php

function uploadImage($dir,$file_name)
{
$target_dir = "images/".$dir."/";
$imageFileType = pathinfo($_FILES["image"]["name"],PATHINFO_EXTENSION);
$target_file = $target_dir . basename($file_name.".".$imageFileType);
$uploadOk = 1;
// Check if image file is a actual image or fake image

    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        write("Please upload an image",true,200);
    }


// Check if file already exists
if (file_exists($target_file)) {

    unlink($target_file);
}
// Check file size
if ($_FILES["image"]["size"] > 500000) {
    $uploadOk = 0;
}
// Allow certain file formats

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    //Error
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
    } else {
    }
}

return $target_file;
}
?>