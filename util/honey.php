<?php
//Model Class for Honey Transactions

function has_honey($user_id,$brand_id,$honey)
{
	global $db;
	
	$honey_db = $db->get("honey",["brand_id","honey"],["AND" => ["user_id" => $user_id,
		"brand_id" => $brand_id]]);

	if(!empty($honey_db))
	{
		$honey_db = (float)$honey_db["honey"];

		if($honey<=$honey_db)
			return true;
		else
			write("Not enough honey",true,200);
	}
	else
		write("No honey in brand",true,200);
}

function has_locked_honey($user_id,$brand_id)
{
	global $db;

	$locked = $db->get("lock",["honey","locked_on"],["AND" => [
		"user_id" => $user_id,
		"brand_id" => $brand_id]]);

	if((!empty($locked)) || ((int)$locked["honey"] != 0))
	{
		return $locked;
	}
	else
		return false;
}

function get_locked_honey($user_id,$brand_id)
{
	global $db;

	$locked = $db->sum("lock","honey",["AND" => [
		"user_id" => $user_id,
		"brand_id" => $brand_id]]);

	return $locked;

}



function add_honey($user_id,$brand_id,$honey)
{
	global $db; 

	$honey_data = $db->get("honey",["honey"],["AND" =>
			["brand_id" => $brand_id,
			"user_id" => $user_id]]);

		if(!empty($honey_data))
		{
			$db->update("honey",["honey[+]" => (float)$honey,
				"#updated_on" => "NOW()"],
				["AND" => ["brand_id" => $brand_id,
				"user_id" => $user_id]]);

			if(!is_db_error())
				return true;
		}
		else
		{
			$db->insert("honey",[
				"user_id" => $user_id,
				"brand_id" => $brand_id,
				"honey" => $honey,
				"#created_on" => "NOW()",
				"#updated_on" => "NOW()"]);

			if(!is_db_error())
				return true;

		}
}

function subtract_honey($user_id,$brand_id,$honey)
{
	global $db;


	$db->update("honey",["honey[-]" => (float)$honey],
		["AND" => [
		"user_id" => $user_id,
		"brand_id" => $brand_id]]);

	if(!is_db_error())
	{
		$honey_table = $db->get("honey",["honey"],["AND" => ["user_id" => $user_id,
				"brand_id" => $brand_id]]);

			if($honey_table["honey"] == 0)
				{
					$db->delete("honey",["AND" => [$user_id,
					"brand_id" => $brand_id]]);
					return true;
				}
	}
}

function lock_honey($user_id,$brand_id,$honey,$locked_for,$link_id)
{
	global $db;

	//Subtract from honey table
	subtract_honey($user_id,$brand_id,$honey);

	$db->insert("lock",[
		"user_id" => $user_id,
		"brand_id" => $brand_id,
		"honey" => $honey,
		"locked_for" => $locked_for,
		"link_id" => $link_id,
		"#locked_on" => "NOW()"]);

	if(!is_db_error())
		return true;
	else
		write("DB Error Occured!",true,200);

	/*
	Depreciated
	if(!is_db_error())
	{
		$lock = $db->get("lock","*",["AND" => ["brand_id" => $brand_id,
			"user_id" => $user_id]]);
		
		if(!empty($lock))
		{
			$db->update("lock",["honey[+]" => (float)$honey,
				"#locked_on" => "NOW()"],["AND" => [
				"user_id" => $user_id,
				"brand_id" => $brand_id]]);
		}
		else
		{

			$db->insert("lock",["user_id" => $user_id,
				"honey" => $honey,
				"brand_id" => $brand_id,
				"#locked_on" => "NOW()"]);
		}


		if(!is_db_error())
			return true;
		else
			write("A DB Error Occured",true,200);
	}
	else
		write("A DB Error Occured",true,200);
		*/

}

function unlock_honey($user_id,$brand_id,$honey,$locked_for,$link_id)
{
	global $db;

	/*$db->update("lock",["honey[-]" => (float)$honey],["AND" => [
		"user_id" => (int)$user_id,
		"brand_id" => (int)$brand_id]]);*/

	$db->update("lock",[
		"is_locked" => 0],["AND" =>
		["user_id" => $user_id,
		"brand_id" => $brand_id,
		"honey" => $honey,
		"locked_for" => $locked_for,
		"link_id" => $link_id
		]]);

	if(!is_db_error())
	{
		add_honey($user_id,$brand_id,$honey);
	}


}

function trade($sender_id,$sent_brand,$sent_honey,$receiver_id,$received_brand,$received_honey)
{
	global $db;

	//Sender
		//Deduct
	subtract_honey($sender_id,$sent_brand,$sent_honey);
	subtract_honey($receiver_id,$received_brand,$received_honey);

	add_honey($sender_id,$received_brand,$received_honey);
	add_honey($receiver_id,$sent_brand,$sent_honey);

	return true;
		//Add

	//Receiver
}



?>