<title>Server Response</title>
<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

$brand_id = $_POST["brand_id"];
$user_id = $_POST["user_id"];
$item_amount = $_POST["item_amount"];
$honey_applied = $_POST["honey_applied"];
$item_id = 0;

$service_url = 'http://api.hivetogether.com/plugin/purchases';

$curl = curl_init($service_url);

$curl_post_data = array(
        'user_id' => $user_id,
        'item_amount' => $item_amount,
        'honey_applied' => $honey_applied,
        'item_id' => $item_id
);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'PluginKey: '.$brand_id
    ));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);

$curl_response = curl_exec($curl);

if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
curl_close($curl);

echo (json_encode($curl_response, JSON_PRETTY_PRINT));

?>
<br>
<a href='test.html'>Go Back</a>